﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Single QRTracking.QRCode::get_PhysicalSize()
extern void QRCode_get_PhysicalSize_m66F4B88FD1B4683509CF2D0996D72559A17A9902 (void);
// 0x00000002 System.Void QRTracking.QRCode::set_PhysicalSize(System.Single)
extern void QRCode_set_PhysicalSize_m94D9689C61ECA90E8D0F13301D977554F3783EAE (void);
// 0x00000003 System.String QRTracking.QRCode::get_CodeText()
extern void QRCode_get_CodeText_m5897A79777458012E3B02F228DBB93F6B5B6CC05 (void);
// 0x00000004 System.Void QRTracking.QRCode::set_CodeText(System.String)
extern void QRCode_set_CodeText_m28B52A6F5439443980135A8EE7E617BA9033DB10 (void);
// 0x00000005 System.Void QRTracking.QRCode::Start()
extern void QRCode_Start_m5B580FD06B29004C00FB82FA59354F714EDACAC7 (void);
// 0x00000006 System.Void QRTracking.QRCode::UpdatePropertiesDisplay()
extern void QRCode_UpdatePropertiesDisplay_m8435EBD3EB160C0766FF745993EC1BC558F5D1B0 (void);
// 0x00000007 System.Void QRTracking.QRCode::Update()
extern void QRCode_Update_mF6C3735CB65E728601F6A77D160CDBB4BD89CDE2 (void);
// 0x00000008 System.Void QRTracking.QRCode::LaunchUri()
extern void QRCode_LaunchUri_m4847C41A64D886FA3AAF7267B08762B8157C3077 (void);
// 0x00000009 System.Void QRTracking.QRCode::OnInputClicked()
extern void QRCode_OnInputClicked_mC8819DDBBC762904290628629842F96FDD739C7F (void);
// 0x0000000A System.Void QRTracking.QRCode::.ctor()
extern void QRCode__ctor_mB1803033A7CD7462427DE70C5D9016F2DBF48265 (void);
// 0x0000000B QRTracking.QRCodeEventArgs`1<TData> QRTracking.QRCodeEventArgs::Create(TData)
// 0x0000000C TData QRTracking.QRCodeEventArgs`1::get_Data()
// 0x0000000D System.Void QRTracking.QRCodeEventArgs`1::set_Data(TData)
// 0x0000000E System.Void QRTracking.QRCodeEventArgs`1::.ctor(TData)
// 0x0000000F System.Boolean QRTracking.QRCodesManager::get_IsTrackerRunning()
extern void QRCodesManager_get_IsTrackerRunning_mC9456AA68F8FEB38955B6EFAB5AF9401743C9654 (void);
// 0x00000010 System.Void QRTracking.QRCodesManager::set_IsTrackerRunning(System.Boolean)
extern void QRCodesManager_set_IsTrackerRunning_m87A35DAA738FDF8A70DC399972A8B949710DE1BE (void);
// 0x00000011 System.Boolean QRTracking.QRCodesManager::get_IsSupported()
extern void QRCodesManager_get_IsSupported_mD232D85C8E3F183817439952F3B698C616251266 (void);
// 0x00000012 System.Void QRTracking.QRCodesManager::set_IsSupported(System.Boolean)
extern void QRCodesManager_set_IsSupported_mF771850C3B75C4129C4507911BD8BAFB4D35579E (void);
// 0x00000013 System.Void QRTracking.QRCodesManager::add_QRCodesTrackingStateChanged(System.EventHandler`1<System.Boolean>)
extern void QRCodesManager_add_QRCodesTrackingStateChanged_m7C56678E4F72DA85322301D5094AD95F6AC1FD3F (void);
// 0x00000014 System.Void QRTracking.QRCodesManager::remove_QRCodesTrackingStateChanged(System.EventHandler`1<System.Boolean>)
extern void QRCodesManager_remove_QRCodesTrackingStateChanged_m633611B6DE3E5453F3A49FE5D961DB9213794F1D (void);
// 0x00000015 System.Void QRTracking.QRCodesManager::add_QRCodeAdded(System.EventHandler`1<QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>>)
extern void QRCodesManager_add_QRCodeAdded_mF721C6A999B870C78469EC476B86498E54466EF0 (void);
// 0x00000016 System.Void QRTracking.QRCodesManager::remove_QRCodeAdded(System.EventHandler`1<QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>>)
extern void QRCodesManager_remove_QRCodeAdded_m44FA402D95E7322D583C1E725262AFFAD4B69E96 (void);
// 0x00000017 System.Void QRTracking.QRCodesManager::add_QRCodeUpdated(System.EventHandler`1<QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>>)
extern void QRCodesManager_add_QRCodeUpdated_m549057F0C2F94DB6395966A8996950FD4AC2F67F (void);
// 0x00000018 System.Void QRTracking.QRCodesManager::remove_QRCodeUpdated(System.EventHandler`1<QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>>)
extern void QRCodesManager_remove_QRCodeUpdated_mB302DB2CF68FBC646191D307A6CA8584495BB1A7 (void);
// 0x00000019 System.Void QRTracking.QRCodesManager::add_QRCodeRemoved(System.EventHandler`1<QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>>)
extern void QRCodesManager_add_QRCodeRemoved_m5E321DB3CCC90465C6D96D65C38589C39A5BD7EC (void);
// 0x0000001A System.Void QRTracking.QRCodesManager::remove_QRCodeRemoved(System.EventHandler`1<QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>>)
extern void QRCodesManager_remove_QRCodeRemoved_mC4F7569BCC35707E7C3373DADC1CE34E639D0E31 (void);
// 0x0000001B System.Guid QRTracking.QRCodesManager::GetIdForQRCode(System.String)
extern void QRCodesManager_GetIdForQRCode_mD7808CDA5C09DA7E0A736C34852DDECD690EE85D (void);
// 0x0000001C System.Collections.Generic.IList`1<Microsoft.MixedReality.QR.QRCode> QRTracking.QRCodesManager::GetList()
extern void QRCodesManager_GetList_mA653A0FC311DF69CF76083F488511CF035B7D879 (void);
// 0x0000001D System.Void QRTracking.QRCodesManager::Awake()
extern void QRCodesManager_Awake_m2C79F04A120B5DAA1D523E7772F5A77576F8F6C6 (void);
// 0x0000001E System.Void QRTracking.QRCodesManager::Start()
extern void QRCodesManager_Start_mF0D5C1F7D9BC766CE19E7984146E8BFCCBA95663 (void);
// 0x0000001F System.Void QRTracking.QRCodesManager::SetupQRTracking()
extern void QRCodesManager_SetupQRTracking_m8FD2FFB3955E2B93509733E0A16023424186B46E (void);
// 0x00000020 System.Void QRTracking.QRCodesManager::StartQRTracking()
extern void QRCodesManager_StartQRTracking_m50E2E3E41E58AF60827FBD97FDCD821FA06C2196 (void);
// 0x00000021 System.Void QRTracking.QRCodesManager::StopQRTracking()
extern void QRCodesManager_StopQRTracking_m3C443CCB477CC6F05DAF1A060C8C04D991A3BD08 (void);
// 0x00000022 System.Void QRTracking.QRCodesManager::QRCodeWatcher_Removed(System.Object,Microsoft.MixedReality.QR.QRCodeRemovedEventArgs)
extern void QRCodesManager_QRCodeWatcher_Removed_m046562E557683C4ECF222C50CEFA331DDB23AC22 (void);
// 0x00000023 System.Void QRTracking.QRCodesManager::QRCodeWatcher_Updated(System.Object,Microsoft.MixedReality.QR.QRCodeUpdatedEventArgs)
extern void QRCodesManager_QRCodeWatcher_Updated_mA551482BB84708688919ACE7D0CB44D684BED184 (void);
// 0x00000024 System.Void QRTracking.QRCodesManager::QRCodeWatcher_Added(System.Object,Microsoft.MixedReality.QR.QRCodeAddedEventArgs)
extern void QRCodesManager_QRCodeWatcher_Added_m9F8E74CCA61562F4ECC6B4A7848091EFA5F2F82F (void);
// 0x00000025 System.Void QRTracking.QRCodesManager::QRCodeWatcher_EnumerationCompleted(System.Object,System.Object)
extern void QRCodesManager_QRCodeWatcher_EnumerationCompleted_mB3C0E086F9BB735FBB366D6C97E0872F3BEB34FA (void);
// 0x00000026 System.Void QRTracking.QRCodesManager::Update()
extern void QRCodesManager_Update_mF3B0628A9F775C7D8BB89D028E067AF692D1B6D0 (void);
// 0x00000027 System.Void QRTracking.QRCodesManager::.ctor()
extern void QRCodesManager__ctor_mD5D7357EF7F36D5CA5B25152B628E93BA1C0BC2C (void);
// 0x00000028 System.Void QRTracking.QRCodesManager/<Start>d__29::MoveNext()
extern void U3CStartU3Ed__29_MoveNext_m072C22EAE854811888D9D7B392A26AEB3C4A61F4 (void);
// 0x00000029 System.Void QRTracking.QRCodesManager/<Start>d__29::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__29_SetStateMachine_mBC2E85E72E5B5D9E58232E0B2D97E371504C5415 (void);
// 0x0000002A System.Void QRTracking.QRCodesSetup::Awake()
extern void QRCodesSetup_Awake_m63B0B8A9055DB6F25B5913CBDC76191C42AC4651 (void);
// 0x0000002B System.Void QRTracking.QRCodesSetup::Start()
extern void QRCodesSetup_Start_mCEEC77A9C80EA9C89759140ADA0FEEB32D483989 (void);
// 0x0000002C System.Void QRTracking.QRCodesSetup::Update()
extern void QRCodesSetup_Update_m5D7D4BF841DF20A39C638D2A8C260AD378330468 (void);
// 0x0000002D System.Void QRTracking.QRCodesSetup::.ctor()
extern void QRCodesSetup__ctor_m653A4F8F95ADBEC614A78D95B28B42685097C528 (void);
// 0x0000002E System.Void QRTracking.QRCodesVisualizer::Awake()
extern void QRCodesVisualizer_Awake_mD7302E1A8F017B284F0B7310C30B367A1FD3BB91 (void);
// 0x0000002F System.Void QRTracking.QRCodesVisualizer::Start()
extern void QRCodesVisualizer_Start_mEA54A691DFE96E709B86AA65A46104B2ECB440E0 (void);
// 0x00000030 System.Void QRTracking.QRCodesVisualizer::Instance_QRCodesTrackingStateChanged(System.Object,System.Boolean)
extern void QRCodesVisualizer_Instance_QRCodesTrackingStateChanged_m3B1EC4ED73F5E209C5578C3B3465AC2B2827740E (void);
// 0x00000031 System.Void QRTracking.QRCodesVisualizer::Instance_QRCodeAdded(System.Object,QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>)
extern void QRCodesVisualizer_Instance_QRCodeAdded_mFAA2A1FE08BB25E07CE84C550BB505029CBC4FF8 (void);
// 0x00000032 System.Void QRTracking.QRCodesVisualizer::Instance_QRCodeUpdated(System.Object,QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>)
extern void QRCodesVisualizer_Instance_QRCodeUpdated_m75474EAB744634849399D205A5E432F30DF0CC5B (void);
// 0x00000033 System.Void QRTracking.QRCodesVisualizer::Instance_QRCodeRemoved(System.Object,QRTracking.QRCodeEventArgs`1<Microsoft.MixedReality.QR.QRCode>)
extern void QRCodesVisualizer_Instance_QRCodeRemoved_m25354327B6CA889F15D757669589CCA96A9BAE75 (void);
// 0x00000034 System.Void QRTracking.QRCodesVisualizer::HandleEvents()
extern void QRCodesVisualizer_HandleEvents_mEAF95056ACDA440FDFE2A906F91896EC29623CCF (void);
// 0x00000035 System.Void QRTracking.QRCodesVisualizer::Update()
extern void QRCodesVisualizer_Update_mA510F4B0B1BB613FB5BBFE59B8AE813C426DDD26 (void);
// 0x00000036 System.Void QRTracking.QRCodesVisualizer::.ctor()
extern void QRCodesVisualizer__ctor_mBF8306BB08618069F4B54B7E9E4350137D5DA556 (void);
// 0x00000037 System.Void QRTracking.QRCodesVisualizer/ActionData::.ctor(QRTracking.QRCodesVisualizer/ActionData/Type,Microsoft.MixedReality.QR.QRCode)
extern void ActionData__ctor_mDC9BFD9DA204A290EC746DEE26D0C182D8C93BEE (void);
// 0x00000038 T QRTracking.Singleton`1::get_Instance()
// 0x00000039 System.Void QRTracking.Singleton`1::OnApplicationQuit()
// 0x0000003A System.Void QRTracking.Singleton`1::OnDestroy()
// 0x0000003B System.Void QRTracking.Singleton`1::.ctor()
// 0x0000003C System.Void QRTracking.Singleton`1::.cctor()
// 0x0000003D System.Guid QRTracking.SpatialGraphCoordinateSystem::get_Id()
extern void SpatialGraphCoordinateSystem_get_Id_m060D4026EF4D36080912F6B5DA3CD6E5E0AF0EA2 (void);
// 0x0000003E System.Void QRTracking.SpatialGraphCoordinateSystem::set_Id(System.Guid)
extern void SpatialGraphCoordinateSystem_set_Id_m3098811F585A485A3DA0AEACE1FB2232F5CFF6FE (void);
// 0x0000003F System.Void QRTracking.SpatialGraphCoordinateSystem::Awake()
extern void SpatialGraphCoordinateSystem_Awake_m371197F59C0225CD7E0DD961ACE4BEC461680F8B (void);
// 0x00000040 System.Void QRTracking.SpatialGraphCoordinateSystem::Start()
extern void SpatialGraphCoordinateSystem_Start_mC3CE71D30472A1616CB8A44A0F904900376A96A3 (void);
// 0x00000041 System.Void QRTracking.SpatialGraphCoordinateSystem::UpdateLocation()
extern void SpatialGraphCoordinateSystem_UpdateLocation_m11D2D1190255DAE4B4F2F922B991504F27B421C5 (void);
// 0x00000042 System.Void QRTracking.SpatialGraphCoordinateSystem::Update()
extern void SpatialGraphCoordinateSystem_Update_m5F549F8B907953AFF6D7F2FF1EF38D546BD2633A (void);
// 0x00000043 System.Void QRTracking.SpatialGraphCoordinateSystem::.ctor()
extern void SpatialGraphCoordinateSystem__ctor_m3CD5CB50637707C4E222D24C8286886A255F00A7 (void);
static Il2CppMethodPointer s_methodPointers[67] = 
{
	QRCode_get_PhysicalSize_m66F4B88FD1B4683509CF2D0996D72559A17A9902,
	QRCode_set_PhysicalSize_m94D9689C61ECA90E8D0F13301D977554F3783EAE,
	QRCode_get_CodeText_m5897A79777458012E3B02F228DBB93F6B5B6CC05,
	QRCode_set_CodeText_m28B52A6F5439443980135A8EE7E617BA9033DB10,
	QRCode_Start_m5B580FD06B29004C00FB82FA59354F714EDACAC7,
	QRCode_UpdatePropertiesDisplay_m8435EBD3EB160C0766FF745993EC1BC558F5D1B0,
	QRCode_Update_mF6C3735CB65E728601F6A77D160CDBB4BD89CDE2,
	QRCode_LaunchUri_m4847C41A64D886FA3AAF7267B08762B8157C3077,
	QRCode_OnInputClicked_mC8819DDBBC762904290628629842F96FDD739C7F,
	QRCode__ctor_mB1803033A7CD7462427DE70C5D9016F2DBF48265,
	NULL,
	NULL,
	NULL,
	NULL,
	QRCodesManager_get_IsTrackerRunning_mC9456AA68F8FEB38955B6EFAB5AF9401743C9654,
	QRCodesManager_set_IsTrackerRunning_m87A35DAA738FDF8A70DC399972A8B949710DE1BE,
	QRCodesManager_get_IsSupported_mD232D85C8E3F183817439952F3B698C616251266,
	QRCodesManager_set_IsSupported_mF771850C3B75C4129C4507911BD8BAFB4D35579E,
	QRCodesManager_add_QRCodesTrackingStateChanged_m7C56678E4F72DA85322301D5094AD95F6AC1FD3F,
	QRCodesManager_remove_QRCodesTrackingStateChanged_m633611B6DE3E5453F3A49FE5D961DB9213794F1D,
	QRCodesManager_add_QRCodeAdded_mF721C6A999B870C78469EC476B86498E54466EF0,
	QRCodesManager_remove_QRCodeAdded_m44FA402D95E7322D583C1E725262AFFAD4B69E96,
	QRCodesManager_add_QRCodeUpdated_m549057F0C2F94DB6395966A8996950FD4AC2F67F,
	QRCodesManager_remove_QRCodeUpdated_mB302DB2CF68FBC646191D307A6CA8584495BB1A7,
	QRCodesManager_add_QRCodeRemoved_m5E321DB3CCC90465C6D96D65C38589C39A5BD7EC,
	QRCodesManager_remove_QRCodeRemoved_mC4F7569BCC35707E7C3373DADC1CE34E639D0E31,
	QRCodesManager_GetIdForQRCode_mD7808CDA5C09DA7E0A736C34852DDECD690EE85D,
	QRCodesManager_GetList_mA653A0FC311DF69CF76083F488511CF035B7D879,
	QRCodesManager_Awake_m2C79F04A120B5DAA1D523E7772F5A77576F8F6C6,
	QRCodesManager_Start_mF0D5C1F7D9BC766CE19E7984146E8BFCCBA95663,
	QRCodesManager_SetupQRTracking_m8FD2FFB3955E2B93509733E0A16023424186B46E,
	QRCodesManager_StartQRTracking_m50E2E3E41E58AF60827FBD97FDCD821FA06C2196,
	QRCodesManager_StopQRTracking_m3C443CCB477CC6F05DAF1A060C8C04D991A3BD08,
	QRCodesManager_QRCodeWatcher_Removed_m046562E557683C4ECF222C50CEFA331DDB23AC22,
	QRCodesManager_QRCodeWatcher_Updated_mA551482BB84708688919ACE7D0CB44D684BED184,
	QRCodesManager_QRCodeWatcher_Added_m9F8E74CCA61562F4ECC6B4A7848091EFA5F2F82F,
	QRCodesManager_QRCodeWatcher_EnumerationCompleted_mB3C0E086F9BB735FBB366D6C97E0872F3BEB34FA,
	QRCodesManager_Update_mF3B0628A9F775C7D8BB89D028E067AF692D1B6D0,
	QRCodesManager__ctor_mD5D7357EF7F36D5CA5B25152B628E93BA1C0BC2C,
	U3CStartU3Ed__29_MoveNext_m072C22EAE854811888D9D7B392A26AEB3C4A61F4,
	U3CStartU3Ed__29_SetStateMachine_mBC2E85E72E5B5D9E58232E0B2D97E371504C5415,
	QRCodesSetup_Awake_m63B0B8A9055DB6F25B5913CBDC76191C42AC4651,
	QRCodesSetup_Start_mCEEC77A9C80EA9C89759140ADA0FEEB32D483989,
	QRCodesSetup_Update_m5D7D4BF841DF20A39C638D2A8C260AD378330468,
	QRCodesSetup__ctor_m653A4F8F95ADBEC614A78D95B28B42685097C528,
	QRCodesVisualizer_Awake_mD7302E1A8F017B284F0B7310C30B367A1FD3BB91,
	QRCodesVisualizer_Start_mEA54A691DFE96E709B86AA65A46104B2ECB440E0,
	QRCodesVisualizer_Instance_QRCodesTrackingStateChanged_m3B1EC4ED73F5E209C5578C3B3465AC2B2827740E,
	QRCodesVisualizer_Instance_QRCodeAdded_mFAA2A1FE08BB25E07CE84C550BB505029CBC4FF8,
	QRCodesVisualizer_Instance_QRCodeUpdated_m75474EAB744634849399D205A5E432F30DF0CC5B,
	QRCodesVisualizer_Instance_QRCodeRemoved_m25354327B6CA889F15D757669589CCA96A9BAE75,
	QRCodesVisualizer_HandleEvents_mEAF95056ACDA440FDFE2A906F91896EC29623CCF,
	QRCodesVisualizer_Update_mA510F4B0B1BB613FB5BBFE59B8AE813C426DDD26,
	QRCodesVisualizer__ctor_mBF8306BB08618069F4B54B7E9E4350137D5DA556,
	ActionData__ctor_mDC9BFD9DA204A290EC746DEE26D0C182D8C93BEE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpatialGraphCoordinateSystem_get_Id_m060D4026EF4D36080912F6B5DA3CD6E5E0AF0EA2,
	SpatialGraphCoordinateSystem_set_Id_m3098811F585A485A3DA0AEACE1FB2232F5CFF6FE,
	SpatialGraphCoordinateSystem_Awake_m371197F59C0225CD7E0DD961ACE4BEC461680F8B,
	SpatialGraphCoordinateSystem_Start_mC3CE71D30472A1616CB8A44A0F904900376A96A3,
	SpatialGraphCoordinateSystem_UpdateLocation_m11D2D1190255DAE4B4F2F922B991504F27B421C5,
	SpatialGraphCoordinateSystem_Update_m5F549F8B907953AFF6D7F2FF1EF38D546BD2633A,
	SpatialGraphCoordinateSystem__ctor_m3CD5CB50637707C4E222D24C8286886A255F00A7,
};
extern void U3CStartU3Ed__29_MoveNext_m072C22EAE854811888D9D7B392A26AEB3C4A61F4_AdjustorThunk (void);
extern void U3CStartU3Ed__29_SetStateMachine_mBC2E85E72E5B5D9E58232E0B2D97E371504C5415_AdjustorThunk (void);
extern void ActionData__ctor_mDC9BFD9DA204A290EC746DEE26D0C182D8C93BEE_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x06000028, U3CStartU3Ed__29_MoveNext_m072C22EAE854811888D9D7B392A26AEB3C4A61F4_AdjustorThunk },
	{ 0x06000029, U3CStartU3Ed__29_SetStateMachine_mBC2E85E72E5B5D9E58232E0B2D97E371504C5415_AdjustorThunk },
	{ 0x06000037, ActionData__ctor_mDC9BFD9DA204A290EC746DEE26D0C182D8C93BEE_AdjustorThunk },
};
static const int32_t s_InvokerIndices[67] = 
{
	692,
	299,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	95,
	31,
	95,
	31,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	2663,
	14,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	27,
	27,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	94,
	27,
	27,
	27,
	23,
	23,
	23,
	62,
	-1,
	-1,
	-1,
	-1,
	-1,
	442,
	989,
	23,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000004, { 2, 1 } },
	{ 0x0200000B, { 3, 4 } },
	{ 0x0600000B, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)2, 48645 },
	{ (Il2CppRGCTXDataType)3, 49843 },
	{ (Il2CppRGCTXDataType)3, 49844 },
	{ (Il2CppRGCTXDataType)2, 49182 },
	{ (Il2CppRGCTXDataType)1, 48673 },
	{ (Il2CppRGCTXDataType)2, 48673 },
	{ (Il2CppRGCTXDataType)3, 49845 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	67,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
};
