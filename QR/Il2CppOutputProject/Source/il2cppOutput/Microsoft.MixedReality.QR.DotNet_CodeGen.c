﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* Delegate_AddRef_mF193494E0821B8267341BD17F8D1F55C6323C649_RuntimeMethod_var;
extern const RuntimeMethod* Delegate_QueryInterface_m28CBEE6454B0F6D54F4190D317C063B5E8DBC054_RuntimeMethod_var;
extern const RuntimeMethod* Delegate_Release_m15D852676BDBCA307BF3420F6E23FC44A5E6B471_RuntimeMethod_var;
extern const RuntimeMethod* EventSource__invoke_m122079F494464119D24BCDDCB5F785018CA461C5_RuntimeMethod_var;
extern const RuntimeMethod* IAsyncOperation__OnCompleted_m20528AE352B798EDDBFD33AA9064E6BFF7EB60C3_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mB1D74745CAC4D3A311F24386C40E32BE690F6399 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsUnmanagedAttribute::.ctor()
extern void IsUnmanagedAttribute__ctor_m019D815CD79A489B614C483183236BE40AA3D07A (void);
// 0x00000003 System.Void WinRT.Platform::.cctor()
extern void Platform__cctor_m4D61405C27FCE515FBC61BC396345E62F5E7BEE1 (void);
// 0x00000004 System.IntPtr WinRT.Platform::TryLoadLibraryExW(System.String,System.IntPtr,System.UInt32)
extern void Platform_TryLoadLibraryExW_mB2820476A3A8B379EF7494738B2A588DE0C47602 (void);
// 0x00000005 System.Void WinRT.Platform::FreeLibrary(System.IntPtr)
extern void Platform_FreeLibrary_mB40A97287729A7D9CA08F019D5E2B3FD2182BADE (void);
// 0x00000006 T WinRT.Platform::TryGetProcAddress(System.IntPtr)
// 0x00000007 T WinRT.Platform::GetProcAddress(System.IntPtr)
// 0x00000008 System.IntPtr WinRT.Platform::CoIncrementMTAUsage()
extern void Platform_CoIncrementMTAUsage_mAB298D3128173B52D499DB013F6E8E6862362B6A (void);
// 0x00000009 System.Void WinRT.Platform::CoDecrementMTAUsage(System.IntPtr)
extern void Platform_CoDecrementMTAUsage_m5C6359F3746212DE040D315FB3E2F82277DFF742 (void);
// 0x0000000A System.IntPtr WinRT.Platform::RoGetActivationFactory(System.IntPtr,System.Guid&)
extern void Platform_RoGetActivationFactory_mAD5908D6ACB3F1C3D9DE7A8C442A39A8C198B7DC (void);
// 0x0000000B System.IntPtr WinRT.Platform::WindowsCreateString(System.String,System.Int32)
extern void Platform_WindowsCreateString_mF7B414388C91EA50DDE89FDA5E43637E78F81534 (void);
// 0x0000000C System.Char* WinRT.Platform::WindowsGetStringRawBuffer(System.IntPtr,System.UInt32*)
extern void Platform_WindowsGetStringRawBuffer_m8E74BB4093C72287451E59A003A823DB3AE239FB (void);
// 0x0000000D System.Void WinRT.Platform::WindowsDeleteString(System.IntPtr)
extern void Platform_WindowsDeleteString_mEFE91DAD454136EFC6F52BD9444FF690ACF5342A (void);
// 0x0000000E System.Int32 WinRT.Platform::GetHRForLastWin32Error()
extern void Platform_GetHRForLastWin32Error_m1FF0B1A7D52C63BFCA22F2058BDB629FF4D37443 (void);
// 0x0000000F System.IntPtr WinRT.Platform/IPlatformLinkage::_LoadLibraryExW(System.String,System.IntPtr,System.UInt32)
// 0x00000010 System.Boolean WinRT.Platform/IPlatformLinkage::_FreeLibrary(System.IntPtr)
// 0x00000011 System.IntPtr WinRT.Platform/IPlatformLinkage::_GetProcAddress(System.IntPtr,System.String)
// 0x00000012 System.Int32 WinRT.Platform/IPlatformLinkage::_CoIncrementMTAUsage(System.IntPtr*)
// 0x00000013 System.Int32 WinRT.Platform/IPlatformLinkage::_CoDecrementMTAUsage(System.IntPtr)
// 0x00000014 System.Int32 WinRT.Platform/IPlatformLinkage::_RoGetActivationFactory(System.IntPtr,System.Guid&,System.IntPtr*)
// 0x00000015 System.Int32 WinRT.Platform/IPlatformLinkage::_WindowsCreateString(System.String,System.Int32,System.IntPtr*)
// 0x00000016 System.Char* WinRT.Platform/IPlatformLinkage::_WindowsGetStringRawBuffer(System.IntPtr,System.UInt32*)
// 0x00000017 System.Int32 WinRT.Platform/IPlatformLinkage::_WindowsDeleteString(System.IntPtr)
// 0x00000018 System.Int32 WinRT.Platform/IPlatformLinkage::_GetHRForLastWin32Error()
// 0x00000019 System.IntPtr WinRT.Platform/DotNETLinkage::_LoadLibraryExW(System.String,System.IntPtr,System.UInt32)
extern void DotNETLinkage__LoadLibraryExW_m57265357C3DFDCF43B21CDDB46D61EDECC386EF7 (void);
// 0x0000001A System.Boolean WinRT.Platform/DotNETLinkage::_FreeLibrary(System.IntPtr)
extern void DotNETLinkage__FreeLibrary_mE2615802B5349AA67A85E760E603CE5CA5EAAD5E (void);
// 0x0000001B System.IntPtr WinRT.Platform/DotNETLinkage::_GetProcAddress(System.IntPtr,System.String)
extern void DotNETLinkage__GetProcAddress_m2C02C031E6FA25474D1420576FC0FE145A4CBE0B (void);
// 0x0000001C System.Int32 WinRT.Platform/DotNETLinkage::_CoIncrementMTAUsage(System.IntPtr*)
extern void DotNETLinkage__CoIncrementMTAUsage_m1150721B485961618B37E292796E46A972F4F928 (void);
// 0x0000001D System.Int32 WinRT.Platform/DotNETLinkage::_CoDecrementMTAUsage(System.IntPtr)
extern void DotNETLinkage__CoDecrementMTAUsage_mC6258F7559A7C6307F4ED78547F0C4D26EE0F0D1 (void);
// 0x0000001E System.Int32 WinRT.Platform/DotNETLinkage::_RoGetActivationFactory(System.IntPtr,System.Guid&,System.IntPtr*)
extern void DotNETLinkage__RoGetActivationFactory_mC56B28FEA0341E4BE794771B17DEF12D9B0407F7 (void);
// 0x0000001F System.Int32 WinRT.Platform/DotNETLinkage::_WindowsCreateString(System.String,System.Int32,System.IntPtr*)
extern void DotNETLinkage__WindowsCreateString_m48005411B4C935BA857A2204AD7CFA39C4A1A99A (void);
// 0x00000020 System.Char* WinRT.Platform/DotNETLinkage::_WindowsGetStringRawBuffer(System.IntPtr,System.UInt32*)
extern void DotNETLinkage__WindowsGetStringRawBuffer_m00B5E990E8330854817F9CD72F93073CCE026DAF (void);
// 0x00000021 System.Int32 WinRT.Platform/DotNETLinkage::_WindowsDeleteString(System.IntPtr)
extern void DotNETLinkage__WindowsDeleteString_m67F959C95FF3766B4383B107D84B74FAD7FDD536 (void);
// 0x00000022 System.Int32 WinRT.Platform/DotNETLinkage::_GetHRForLastWin32Error()
extern void DotNETLinkage__GetHRForLastWin32Error_mA31EFCA52B9F624FCB75AAC06A35DBBF720FC217 (void);
// 0x00000023 System.IntPtr WinRT.Platform/DotNETLinkage::LoadLibraryExW(System.String,System.IntPtr,System.UInt32)
extern void DotNETLinkage_LoadLibraryExW_m9A367A0B52A833F9C0291AFC496C26F996C76427 (void);
// 0x00000024 System.Boolean WinRT.Platform/DotNETLinkage::FreeLibrary(System.IntPtr)
extern void DotNETLinkage_FreeLibrary_mC09C5A3F3659FC0E217530199A0F3AEDC706DE6B (void);
// 0x00000025 System.IntPtr WinRT.Platform/DotNETLinkage::GetProcAddress(System.IntPtr,System.String)
extern void DotNETLinkage_GetProcAddress_m7D524CC8A320B481AE548AE593A42BF26FF0E7D5 (void);
// 0x00000026 System.Int32 WinRT.Platform/DotNETLinkage::CoIncrementMTAUsage(System.IntPtr*)
extern void DotNETLinkage_CoIncrementMTAUsage_mE118702CCC99AE5C567185B089EF3C7365B4111E (void);
// 0x00000027 System.Int32 WinRT.Platform/DotNETLinkage::CoDecrementMTAUsage(System.IntPtr)
extern void DotNETLinkage_CoDecrementMTAUsage_mB6BFEF95BED2ADFD7B418C4901391FCDAA0022C1 (void);
// 0x00000028 System.Int32 WinRT.Platform/DotNETLinkage::RoGetActivationFactory(System.IntPtr,System.Guid&,System.IntPtr*)
extern void DotNETLinkage_RoGetActivationFactory_mB6FC97CA6B844D391CAD990AE950977117CB819A (void);
// 0x00000029 System.Int32 WinRT.Platform/DotNETLinkage::WindowsCreateString(System.String,System.Int32,System.IntPtr*)
extern void DotNETLinkage_WindowsCreateString_m843371AB0F6853FAF67FE9AD83C1FBC4CE8423EA (void);
// 0x0000002A System.Char* WinRT.Platform/DotNETLinkage::WindowsGetStringRawBuffer(System.IntPtr,System.UInt32*)
extern void DotNETLinkage_WindowsGetStringRawBuffer_m1223DC994D7EB97063CC8F6DAB77D8DA0242B268 (void);
// 0x0000002B System.Int32 WinRT.Platform/DotNETLinkage::WindowsDeleteString(System.IntPtr)
extern void DotNETLinkage_WindowsDeleteString_mCC1A08CAA3C34FCF98B72BAD78E48A5A38F1D8DD (void);
// 0x0000002C System.Void WinRT.Platform/DotNETLinkage::.ctor()
extern void DotNETLinkage__ctor_mB074F3B340B74F9367FA09CA0F1F899C5A483466 (void);
// 0x0000002D System.IntPtr WinRT.Platform/IL2CPPLinkage::_LoadLibraryExW(System.String,System.IntPtr,System.UInt32)
extern void IL2CPPLinkage__LoadLibraryExW_mEE765D0163357934C9FFDF577956814601A869C4 (void);
// 0x0000002E System.Boolean WinRT.Platform/IL2CPPLinkage::_FreeLibrary(System.IntPtr)
extern void IL2CPPLinkage__FreeLibrary_m4F8F399A4BD1CA05F5B616515F469E56C6EFCC4D (void);
// 0x0000002F System.IntPtr WinRT.Platform/IL2CPPLinkage::_GetProcAddress(System.IntPtr,System.String)
extern void IL2CPPLinkage__GetProcAddress_m5878AA4B9AAF72AD1773F6CDF49D2D43D023BDC9 (void);
// 0x00000030 System.Int32 WinRT.Platform/IL2CPPLinkage::_CoIncrementMTAUsage(System.IntPtr*)
extern void IL2CPPLinkage__CoIncrementMTAUsage_mD9780DD9222D198C9C1E5D718689EC44889FFEDD (void);
// 0x00000031 System.Int32 WinRT.Platform/IL2CPPLinkage::_CoDecrementMTAUsage(System.IntPtr)
extern void IL2CPPLinkage__CoDecrementMTAUsage_mA6085E341D402E5C841239F5E8729541415557C9 (void);
// 0x00000032 System.Int32 WinRT.Platform/IL2CPPLinkage::_RoGetActivationFactory(System.IntPtr,System.Guid&,System.IntPtr*)
extern void IL2CPPLinkage__RoGetActivationFactory_mD9B64895F07EE68FD4BE0408F2E3C45C5E813FFF (void);
// 0x00000033 System.Int32 WinRT.Platform/IL2CPPLinkage::_WindowsCreateString(System.String,System.Int32,System.IntPtr*)
extern void IL2CPPLinkage__WindowsCreateString_m35546AF02FCD01FF468E130AAA66CBF5F1A39C58 (void);
// 0x00000034 System.Char* WinRT.Platform/IL2CPPLinkage::_WindowsGetStringRawBuffer(System.IntPtr,System.UInt32*)
extern void IL2CPPLinkage__WindowsGetStringRawBuffer_mB9DF308A4B75EC602FAB5FA66BA6C418F1AA3668 (void);
// 0x00000035 System.Int32 WinRT.Platform/IL2CPPLinkage::_WindowsDeleteString(System.IntPtr)
extern void IL2CPPLinkage__WindowsDeleteString_m0B3377B15784475F3EBE705FBA60665F70BCB52B (void);
// 0x00000036 System.Int32 WinRT.Platform/IL2CPPLinkage::_GetHRForLastWin32Error()
extern void IL2CPPLinkage__GetHRForLastWin32Error_mDF1B3A8650831F0DD3A99A78648CF0B945A7664A (void);
// 0x00000037 System.IntPtr WinRT.Platform/IL2CPPLinkage::LoadLibraryExW(System.String,System.IntPtr,System.UInt32)
extern void IL2CPPLinkage_LoadLibraryExW_mC685B6B56B732D1A710BA06AC0849F6BBD6283F6 (void);
// 0x00000038 System.Boolean WinRT.Platform/IL2CPPLinkage::FreeLibrary(System.IntPtr)
extern void IL2CPPLinkage_FreeLibrary_m71CA7FAB535475C4178AF8EA5526F0247F0AFB85 (void);
// 0x00000039 System.IntPtr WinRT.Platform/IL2CPPLinkage::GetProcAddress(System.IntPtr,System.String)
extern void IL2CPPLinkage_GetProcAddress_mC672CC99DBA693AAA988C599885974F3153E07F3 (void);
// 0x0000003A System.Int32 WinRT.Platform/IL2CPPLinkage::CoIncrementMTAUsage(System.IntPtr*)
extern void IL2CPPLinkage_CoIncrementMTAUsage_m3AC500BB7A8303FC8A01855FA1CFD78BC15EBD08 (void);
// 0x0000003B System.Int32 WinRT.Platform/IL2CPPLinkage::CoDecrementMTAUsage(System.IntPtr)
extern void IL2CPPLinkage_CoDecrementMTAUsage_mD2E7E2194EFA0D41C486435911ED39FA10E80F3D (void);
// 0x0000003C System.Int32 WinRT.Platform/IL2CPPLinkage::RoGetActivationFactory(System.IntPtr,System.Guid&,System.IntPtr*)
extern void IL2CPPLinkage_RoGetActivationFactory_m910D66A79FFD913A25B2A880CD89BEA3A70FD028 (void);
// 0x0000003D System.Int32 WinRT.Platform/IL2CPPLinkage::WindowsCreateString(System.String,System.Int32,System.IntPtr*)
extern void IL2CPPLinkage_WindowsCreateString_m4233064DE1FAD27B3EA41149F96857B1B0D8589B (void);
// 0x0000003E System.Char* WinRT.Platform/IL2CPPLinkage::WindowsGetStringRawBuffer(System.IntPtr,System.UInt32*)
extern void IL2CPPLinkage_WindowsGetStringRawBuffer_mB65B56D4E11B9A88D352DD0E1160B4FC158FC1E4 (void);
// 0x0000003F System.Int32 WinRT.Platform/IL2CPPLinkage::WindowsDeleteString(System.IntPtr)
extern void IL2CPPLinkage_WindowsDeleteString_mF7B2093CA919DBAEB8AE95886A9D21E11D00766A (void);
// 0x00000040 System.Void WinRT.Platform/IL2CPPLinkage::.ctor()
extern void IL2CPPLinkage__ctor_mAAA91BAD42E2A20E2921BE2A1CF558AA45F4C489 (void);
// 0x00000041 System.IntPtr WinRT.Mono::mono_thread_current()
extern void Mono_mono_thread_current_mB8EA4881B220B2B07CBCE6AD3F3B4E035A2B3B37 (void);
// 0x00000042 System.Boolean WinRT.Mono::mono_thread_is_foreign(System.IntPtr)
extern void Mono_mono_thread_is_foreign_m532A9C01D3C8DE57846641416A5EB6CC5CBE3707 (void);
// 0x00000043 System.Void WinRT.Mono::.cctor()
extern void Mono__cctor_mD78AA90AD75DBB2269B75A1DCB31283735350578 (void);
// 0x00000044 System.Void WinRT.Mono/mono_thread_cleanup_initialize::.ctor(System.Object,System.IntPtr)
extern void mono_thread_cleanup_initialize__ctor_mD97F789C38AB8E6D4C31E902622E29CECF71C23B (void);
// 0x00000045 System.Void WinRT.Mono/mono_thread_cleanup_initialize::Invoke(System.IntPtr*,System.IntPtr*)
extern void mono_thread_cleanup_initialize_Invoke_m506DF1E7B280E525DAE60B58BB941B3AC03350C3 (void);
// 0x00000046 System.IAsyncResult WinRT.Mono/mono_thread_cleanup_initialize::BeginInvoke(System.IntPtr*,System.IntPtr*,System.AsyncCallback,System.Object)
extern void mono_thread_cleanup_initialize_BeginInvoke_m42FAB2BC583B72B7F8ED4F76CC0E5C39DE8FCE7C (void);
// 0x00000047 System.Void WinRT.Mono/mono_thread_cleanup_initialize::EndInvoke(System.IAsyncResult)
extern void mono_thread_cleanup_initialize_EndInvoke_m153757EE9525E0E15292481E4EE42B3F793B6E1E (void);
// 0x00000048 System.Void WinRT.Mono/mono_thread_cleanup_register::.ctor(System.Object,System.IntPtr)
extern void mono_thread_cleanup_register__ctor_m2D9E73E4D69D312AABAD5D6A3C7E76C3CAA6F5E4 (void);
// 0x00000049 System.Void WinRT.Mono/mono_thread_cleanup_register::Invoke(System.IntPtr)
extern void mono_thread_cleanup_register_Invoke_m3C2E54E14802CA53B60FF8EFEB4062D9F42B7F3D (void);
// 0x0000004A System.IAsyncResult WinRT.Mono/mono_thread_cleanup_register::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void mono_thread_cleanup_register_BeginInvoke_mF701B4C4D09AA7ECC3C9D24F9E1C3C8D7855861F (void);
// 0x0000004B System.Void WinRT.Mono/mono_thread_cleanup_register::EndInvoke(System.IAsyncResult)
extern void mono_thread_cleanup_register_EndInvoke_m8559864DE7F481041F92ED12D3147DDF442E260D (void);
// 0x0000004C System.Void WinRT.Mono/ThreadContext::.cctor()
extern void ThreadContext__cctor_mF106BE5E551303EE7D66B2171D8E6CC83DB6F23F (void);
// 0x0000004D System.Void WinRT.Mono/ThreadContext::.ctor()
extern void ThreadContext__ctor_m82841A2030C09E0334445CC697481418FD0BAB83 (void);
// 0x0000004E System.Void WinRT.Mono/ThreadContext::Dispose()
extern void ThreadContext_Dispose_m8A8F13BA3512314E45EFA48FA4E14D5A813D6B25 (void);
// 0x0000004F System.Void WinRT.Mono/ThreadContext::RegisterPossiblyNonForeignThread()
extern void ThreadContext_RegisterPossiblyNonForeignThread_mEE6D432A9E28F666229782C4E6860A6808C5F095 (void);
// 0x00000050 System.Void WinRT.Mono/<>c::.cctor()
extern void U3CU3Ec__cctor_m66FE8C536731096F510810716A267DC6C062982C (void);
// 0x00000051 System.Void WinRT.Mono/<>c::.ctor()
extern void U3CU3Ec__ctor_m6B00E9E2B08B783B313649A2D4D6002AB33294DA (void);
// 0x00000052 System.Boolean WinRT.Mono/<>c::<.cctor>b__7_0()
extern void U3CU3Ec_U3C_cctorU3Eb__7_0_m37576E4BE02F6CA6E17343992B3C084D76F8A1CE (void);
// 0x00000053 System.Void WinRT.HString::.ctor(System.String)
extern void HString__ctor_m54AF7CF8C00F028D556D223C6407BCF9C54216A1 (void);
// 0x00000054 System.Void WinRT.HString::.ctor(System.IntPtr&)
extern void HString__ctor_m58FE025116D93D2212CA985FD43D37FCD7FCCAF4 (void);
// 0x00000055 System.String WinRT.HString::ToString()
extern void HString_ToString_m3067C77765233CFA5DABACD0A81974B9353F7DE9 (void);
// 0x00000056 System.Void WinRT.HString::Dispose()
extern void HString_Dispose_mE78C86E9299020D162C80AE00E8D8E12E21AFED6 (void);
// 0x00000057 System.Int32* WinRT.ModuleReference::AllocateRefCount()
extern void ModuleReference_AllocateRefCount_m9504F540CF8B75AD5437A4FF92839BED7FD68667 (void);
// 0x00000058 WinRT.ModuleReference WinRT.ModuleReference::Allocate(System.Object,System.IntPtr&)
extern void ModuleReference_Allocate_m7C319500124AE2925324756B62DABA2F0F32EBCB (void);
// 0x00000059 WinRT.ModuleReference WinRT.ModuleReference::get_Null()
extern void ModuleReference_get_Null_m1DB8ADCC56ECF811AFDFA4B8A285A76D58F6E021 (void);
// 0x0000005A System.Void WinRT.ModuleReference::.ctor(System.Object,System.Int32*,System.IntPtr)
extern void ModuleReference__ctor_mD5D1FE57CCD13CDF667A138B5B1D1052474F18F6 (void);
// 0x0000005B WinRT.ModuleReference WinRT.ModuleReference::AddRef()
extern void ModuleReference_AddRef_m5136A0E46D35FF37C8A21CE63E3ED579143ADDD5 (void);
// 0x0000005C System.Void WinRT.ModuleReference::Release()
extern void ModuleReference_Release_mEC3B2EBE3AC8A98FF9EE3B1F7F22F07D0252C48F (void);
// 0x0000005D WinRT.Interop.IUnknownVftbl WinRT.IObjectReference::get_VftblIUnknown()
extern void IObjectReference_get_VftblIUnknown_m5279C0FFD5E238BEA15698C21165E061D791D588 (void);
// 0x0000005E System.Void WinRT.IObjectReference::.ctor(WinRT.ModuleReference,System.IntPtr)
extern void IObjectReference__ctor_m345B807A7F725DA0C786C31FB6FCC74DDA1B69B8 (void);
// 0x0000005F WinRT.ObjectReference`1<T> WinRT.IObjectReference::As()
// 0x00000060 WinRT.ObjectReference`1<T> WinRT.IObjectReference::As(System.Guid)
// 0x00000061 System.Void WinRT.IObjectReference::Finalize()
extern void IObjectReference_Finalize_mFA07314332B5CB3E058326802B56400AEE803F74 (void);
// 0x00000062 System.Boolean WinRT.IObjectReference::get_IsLive()
// 0x00000063 WinRT.Interop.IUnknownVftbl WinRT.ObjectReference`1::get_VftblIUnknown()
// 0x00000064 WinRT.ObjectReference`1<T> WinRT.ObjectReference`1::Attach(WinRT.ModuleReference,System.IntPtr&)
// 0x00000065 WinRT.ObjectReference`1<T> WinRT.ObjectReference`1::FromNativePtrNoRef(System.IntPtr)
// 0x00000066 System.Void WinRT.ObjectReference`1::.ctor(WinRT.ModuleReference,System.IntPtr,System.Boolean)
// 0x00000067 System.Void WinRT.ObjectReference`1::Finalize()
// 0x00000068 System.Boolean WinRT.ObjectReference`1::get_IsLive()
// 0x00000069 System.Void WinRT.DllModule::.cctor()
extern void DllModule__cctor_mAA19375698230E3B8B60DAD54F53287D0746E3F5 (void);
// 0x0000006A WinRT.DllModule WinRT.DllModule::TryLoad(System.String)
extern void DllModule_TryLoad_mD630F812F1BB2488BC47EB3C5489AD09E7375DC3 (void);
// 0x0000006B System.Void WinRT.DllModule::.ctor(System.String,System.IntPtr&,WinRT.DllModule/DllGetActivationFactory)
extern void DllModule__ctor_m39518967C67DEDC1B3957B418077792DDEB3A252 (void);
// 0x0000006C WinRT.ObjectReference`1<WinRT.Interop.IActivationFactoryVftbl> WinRT.DllModule::GetActivationFactory(WinRT.HString)
extern void DllModule_GetActivationFactory_mAE91D0B4A53B6D65EB24F967C61559BA2259E615 (void);
// 0x0000006D System.Void WinRT.DllModule::Finalize()
extern void DllModule_Finalize_mD6E7D99BDD6EBD66342B3B3E1C732263D1F44FDB (void);
// 0x0000006E System.Void WinRT.DllModule/DllGetActivationFactory::.ctor(System.Object,System.IntPtr)
extern void DllGetActivationFactory__ctor_m8AB17E1A8A01BC5D47F9FFC15E320A37B8E6B866 (void);
// 0x0000006F System.Int32 WinRT.DllModule/DllGetActivationFactory::Invoke(System.IntPtr,System.IntPtr*)
extern void DllGetActivationFactory_Invoke_m295F421E5BC950F118C574B619F96F5E3B9E7529 (void);
// 0x00000070 System.IAsyncResult WinRT.DllModule/DllGetActivationFactory::BeginInvoke(System.IntPtr,System.IntPtr*,System.AsyncCallback,System.Object)
extern void DllGetActivationFactory_BeginInvoke_m7F8C6F01BBF80C8D4B54874373288EFE308DE4BF (void);
// 0x00000071 System.Int32 WinRT.DllModule/DllGetActivationFactory::EndInvoke(System.IAsyncResult)
extern void DllGetActivationFactory_EndInvoke_m24A18C458A1DC34EC41D7174293E6862EEF3D4E4 (void);
// 0x00000072 System.Void WinRT.WeakLazy`1::.ctor()
// 0x00000073 System.Void WinRT.WeakLazy`1::.ctor(System.Func`1<T>)
// 0x00000074 T WinRT.WeakLazy`1::get_Value()
// 0x00000075 System.Void WinRT.WeakLazy`1/<>c::.cctor()
// 0x00000076 System.Void WinRT.WeakLazy`1/<>c::.ctor()
// 0x00000077 T WinRT.WeakLazy`1/<>c::<.ctor>b__2_0()
// 0x00000078 WinRT.ModuleReference WinRT.WinrtModule::get_Instance()
extern void WinrtModule_get_Instance_m2065DD5CF15F1673BAC8A50329CF20E3590455DC (void);
// 0x00000079 System.Void WinRT.WinrtModule::.ctor()
extern void WinrtModule__ctor_mCCB8A9F71AEEC91E94D6CB71F2FDA285759CF751 (void);
// 0x0000007A WinRT.ObjectReference`1<WinRT.Interop.IActivationFactoryVftbl> WinRT.WinrtModule::GetActivationFactory(WinRT.HString)
extern void WinrtModule_GetActivationFactory_m1EFCA4645510CE8AC772D33664C8271B43BFA110 (void);
// 0x0000007B System.Void WinRT.WinrtModule::Finalize()
extern void WinrtModule_Finalize_mDDC9B93E0CEBB76E7D0D5363D7CC7AED57B45D56 (void);
// 0x0000007C System.Void WinRT.WinrtModule::.cctor()
extern void WinrtModule__cctor_m4C4F8589C5F94E8DDA5DEB2C38F8E0371BF89610 (void);
// 0x0000007D System.Void WinRT.ActivationFactory`1::.ctor()
// 0x0000007E WinRT.ObjectReference`1<I> WinRT.ActivationFactory`1::_ActivateInstance()
// 0x0000007F WinRT.ObjectReference`1<I> WinRT.ActivationFactory`1::_As()
// 0x00000080 WinRT.ObjectReference`1<I> WinRT.ActivationFactory`1::As()
// 0x00000081 WinRT.ObjectReference`1<I> WinRT.ActivationFactory`1::ActivateInstance()
// 0x00000082 System.Void WinRT.ActivationFactory`1::.cctor()
// 0x00000083 System.Void WinRT.ActivationFactory`1/<>c::.cctor()
// 0x00000084 System.Void WinRT.ActivationFactory`1/<>c::.ctor()
// 0x00000085 System.Boolean WinRT.ActivationFactory`1/<>c::<.cctor>b__8_0()
// 0x00000086 WinRT.Delegate WinRT.Delegate::FindObject(System.IntPtr)
extern void Delegate_FindObject_mFA0830E73AEB1315518CF293E7F98281850DD455 (void);
// 0x00000087 System.Int32 WinRT.Delegate::QueryInterface(System.IntPtr,System.Guid*,System.IntPtr*)
extern void Delegate_QueryInterface_m28CBEE6454B0F6D54F4190D317C063B5E8DBC054 (void);
// 0x00000088 System.UInt32 WinRT.Delegate::AddRef(System.IntPtr)
extern void Delegate_AddRef_mF193494E0821B8267341BD17F8D1F55C6323C649 (void);
// 0x00000089 System.UInt32 WinRT.Delegate::Release(System.IntPtr)
extern void Delegate_Release_m15D852676BDBCA307BF3420F6E23FC44A5E6B471 (void);
// 0x0000008A System.Int32 WinRT.Delegate::QueryInterface(System.Guid*,System.IntPtr*)
extern void Delegate_QueryInterface_m6467DA44D2AAC6A55349DC013423E3F9618B3D12 (void);
// 0x0000008B System.UInt32 WinRT.Delegate::AddRef()
extern void Delegate_AddRef_m1565DF1BCF72D5ABA3E3FA9BA795D02D576A186D (void);
// 0x0000008C System.UInt32 WinRT.Delegate::Release()
extern void Delegate_Release_mDFACF297B26CCB4EE0FA99927B81568B13099712 (void);
// 0x0000008D System.Int32 WinRT.Delegate::MarshalInvoke(System.IntPtr,System.Action`1<T>)
// 0x0000008E System.Void WinRT.Delegate::.cctor()
extern void Delegate__cctor_mA76D7C7A3730B353DC79522007A81D158289706D (void);
// 0x0000008F System.Void WinRT.Delegate::.ctor(System.Guid,System.IntPtr,System.Object)
extern void Delegate__ctor_mC3882A4DA0657F017EC039901A0D03BFC2B3ED7E (void);
// 0x00000090 System.Void WinRT.Delegate::Finalize()
extern void Delegate_Finalize_m3859A6A93825D86B75B3569E2E8D6FE599C636DC (void);
// 0x00000091 System.Void WinRT.Delegate::_Dispose()
extern void Delegate__Dispose_mDBB0DF139255C2DBB123AF2C4CF42FE060423EB3 (void);
// 0x00000092 System.IntPtr WinRT.Delegate/InitialReference::get_DelegatePtr()
extern void InitialReference_get_DelegatePtr_mCBEFA2C039F64556D7C2B80ACBF57EA998B92302 (void);
// 0x00000093 System.Void WinRT.Delegate/InitialReference::.ctor(System.Guid,System.IntPtr,System.Object)
extern void InitialReference__ctor_m7D478E29D9482CBF95C7D1430F13261E36B527F2 (void);
// 0x00000094 System.Void WinRT.Delegate/InitialReference::Finalize()
extern void InitialReference_Finalize_m544A4D8DDB1BC2F826D57AA8E5320F2C46C2F507 (void);
// 0x00000095 System.Void WinRT.Delegate/InitialReference::Dispose()
extern void InitialReference_Dispose_m0C431CDD0BDA7B71144D9E4CEA0EF44D246C6778 (void);
// 0x00000096 System.Int32 WinRT.EventSource::_invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern void EventSource__invoke_m122079F494464119D24BCDDCB5F785018CA461C5 (void);
// 0x00000097 System.Void WinRT.EventSource::.ctor()
extern void EventSource__ctor_mF1DAA42E58CEC58571516A8E2CEAA5034E4759E2 (void);
// 0x00000098 System.Void WinRT.EventSource::.cctor()
extern void EventSource__cctor_m200EE6B9CBAC6F6A5C1B4146AE7C1C464198C248 (void);
// 0x00000099 System.Void WinRT.EventSource/_Invoke::.ctor(System.Object,System.IntPtr)
extern void _Invoke__ctor_m96320F48526CDD119B7B64EEDE72CD8CCD9E896E (void);
// 0x0000009A System.Void WinRT.EventSource/_Invoke::Invoke(System.IntPtr,System.IntPtr)
extern void _Invoke_Invoke_m67B75423B6F02F21276698C37708D10FB53A38AC (void);
// 0x0000009B System.IAsyncResult WinRT.EventSource/_Invoke::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern void _Invoke_BeginInvoke_mF36C5FE792DB02342672AB205181C33C3FFE2206 (void);
// 0x0000009C System.Void WinRT.EventSource/_Invoke::EndInvoke(System.IAsyncResult)
extern void _Invoke_EndInvoke_m2D42659A23F517777933C61E7D95947857447F30 (void);
// 0x0000009D System.Void WinRT.EventSource/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m22350EA1ABD075E9B82A524FE2E6A2A030A2B569 (void);
// 0x0000009E System.Void WinRT.EventSource/<>c__DisplayClass1_0::<_invoke>b__0(WinRT.EventSource/_Invoke)
extern void U3CU3Ec__DisplayClass1_0_U3C_invokeU3Eb__0_m35FD60471B7BC183C27E1188E84018000C5A6F42 (void);
// 0x0000009F System.Void WinRT.EventSource`3::add__event(System.EventHandler`1<A>)
// 0x000000A0 System.Void WinRT.EventSource`3::remove__event(System.EventHandler`1<A>)
// 0x000000A1 System.Void WinRT.EventSource`3::add_Event(System.EventHandler`1<A>)
// 0x000000A2 System.Void WinRT.EventSource`3::remove_Event(System.EventHandler`1<A>)
// 0x000000A3 System.Void WinRT.EventSource`3::.ctor(WinRT.IObjectReference,WinRT.Interop._add_EventHandler,WinRT.Interop._remove_EventHandler,WinRT.EventSource`3/UnmarshalArgs<I,S,A>)
// 0x000000A4 System.Void WinRT.EventSource`3::Finalize()
// 0x000000A5 System.Void WinRT.EventSource`3::Invoke(System.IntPtr,System.IntPtr)
// 0x000000A6 System.Void WinRT.EventSource`3::_Unsubscribe()
// 0x000000A7 System.Void WinRT.EventSource`3/UnmarshalArgs::.ctor(System.Object,System.IntPtr)
// 0x000000A8 A WinRT.EventSource`3/UnmarshalArgs::Invoke(System.IntPtr)
// 0x000000A9 System.IAsyncResult WinRT.EventSource`3/UnmarshalArgs::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
// 0x000000AA A WinRT.EventSource`3/UnmarshalArgs::EndInvoke(System.IAsyncResult)
// 0x000000AB System.Void WinRT.Interop.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_m915A08C30E913EFB4BC61E6E9F483A61A336BD9F (void);
// 0x000000AC System.Void WinRT.Interop.MonoPInvokeCallbackAttribute::set_DelegateType(System.Type)
extern void MonoPInvokeCallbackAttribute_set_DelegateType_mE2444C6D7B8CB7CEB07566E7FB01784F9712F9FA (void);
// 0x000000AD System.Void WinRT.Interop.IUnknownVftbl/_QueryInterface::.ctor(System.Object,System.IntPtr)
extern void _QueryInterface__ctor_mD68BF814E2217323EC027913028D8CB8B36088C8 (void);
// 0x000000AE System.Int32 WinRT.Interop.IUnknownVftbl/_QueryInterface::Invoke(System.IntPtr,System.Guid*,System.IntPtr*)
extern void _QueryInterface_Invoke_m7F69C0E3D2A4B387CB35852201325A59138A6F9C (void);
// 0x000000AF System.IAsyncResult WinRT.Interop.IUnknownVftbl/_QueryInterface::BeginInvoke(System.IntPtr,System.Guid*,System.IntPtr*,System.AsyncCallback,System.Object)
extern void _QueryInterface_BeginInvoke_mA65FC1BEF9A8BDF9B09D72068CC58360081AB038 (void);
// 0x000000B0 System.Int32 WinRT.Interop.IUnknownVftbl/_QueryInterface::EndInvoke(System.IAsyncResult)
extern void _QueryInterface_EndInvoke_m385BF355FE1242A895E6875856F815461B5C202A (void);
// 0x000000B1 System.Void WinRT.Interop.IUnknownVftbl/_AddRef::.ctor(System.Object,System.IntPtr)
extern void _AddRef__ctor_m60659BD2453C94835DA466611328E42B16425A2D (void);
// 0x000000B2 System.UInt32 WinRT.Interop.IUnknownVftbl/_AddRef::Invoke(System.IntPtr)
extern void _AddRef_Invoke_m2AF40C1C641ED0FA34BBD81567ABA691DA994C70 (void);
// 0x000000B3 System.IAsyncResult WinRT.Interop.IUnknownVftbl/_AddRef::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _AddRef_BeginInvoke_m791C36E95C29666B4C024B367F2367CAEE05D403 (void);
// 0x000000B4 System.UInt32 WinRT.Interop.IUnknownVftbl/_AddRef::EndInvoke(System.IAsyncResult)
extern void _AddRef_EndInvoke_mAA71F0A1BAACA74349B5F557F7B090444DD37BF7 (void);
// 0x000000B5 System.Void WinRT.Interop.IUnknownVftbl/_Release::.ctor(System.Object,System.IntPtr)
extern void _Release__ctor_mE49ED0E9447B8425E065438576CBEBEF124A6FB4 (void);
// 0x000000B6 System.UInt32 WinRT.Interop.IUnknownVftbl/_Release::Invoke(System.IntPtr)
extern void _Release_Invoke_m01B8A4F72B103E72CBC07062F5B8C779F6BAF9F5 (void);
// 0x000000B7 System.IAsyncResult WinRT.Interop.IUnknownVftbl/_Release::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _Release_BeginInvoke_mCCEF6D5A6426A68C514CECCEAEC07F2430FDDBBF (void);
// 0x000000B8 System.UInt32 WinRT.Interop.IUnknownVftbl/_Release::EndInvoke(System.IAsyncResult)
extern void _Release_EndInvoke_mB157310C130D824021B7923C15A515CF95D80287 (void);
// 0x000000B9 System.Void WinRT.Interop.IInspectableVftbl/_GetIids::.ctor(System.Object,System.IntPtr)
extern void _GetIids__ctor_m3927D8323DF10F42D92BB19B6F3913B3EB3F377A (void);
// 0x000000BA System.Int32 WinRT.Interop.IInspectableVftbl/_GetIids::Invoke(System.IntPtr,System.UInt32*,System.IntPtr*)
extern void _GetIids_Invoke_mB460E8E2BC2DDE592B667E88D50B6188619479A8 (void);
// 0x000000BB System.IAsyncResult WinRT.Interop.IInspectableVftbl/_GetIids::BeginInvoke(System.IntPtr,System.UInt32*,System.IntPtr*,System.AsyncCallback,System.Object)
extern void _GetIids_BeginInvoke_m0223A3CC43D725E866E965030F89EB0E06962479 (void);
// 0x000000BC System.Int32 WinRT.Interop.IInspectableVftbl/_GetIids::EndInvoke(System.IAsyncResult)
extern void _GetIids_EndInvoke_mA1619393B649307371C51D039138DFE938E81264 (void);
// 0x000000BD System.Void WinRT.Interop.IInspectableVftbl/_GetRuntimeClassName::.ctor(System.Object,System.IntPtr)
extern void _GetRuntimeClassName__ctor_m7DBA37FEDE9D059EF0CC6A7E464F4652DDB585BF (void);
// 0x000000BE System.Int32 WinRT.Interop.IInspectableVftbl/_GetRuntimeClassName::Invoke(System.IntPtr,System.IntPtr*)
extern void _GetRuntimeClassName_Invoke_m4D73D5F05B166C9E76C9806EF9E59D1B472BD188 (void);
// 0x000000BF System.IAsyncResult WinRT.Interop.IInspectableVftbl/_GetRuntimeClassName::BeginInvoke(System.IntPtr,System.IntPtr*,System.AsyncCallback,System.Object)
extern void _GetRuntimeClassName_BeginInvoke_mE8B4EB3D32D2944F22E03F06700C65060616C1CB (void);
// 0x000000C0 System.Int32 WinRT.Interop.IInspectableVftbl/_GetRuntimeClassName::EndInvoke(System.IAsyncResult)
extern void _GetRuntimeClassName_EndInvoke_mEBC65041A6B5B2EDC531D9EC0F0EACF9117FCD85 (void);
// 0x000000C1 System.Void WinRT.Interop.IInspectableVftbl/_GetTrustLevel::.ctor(System.Object,System.IntPtr)
extern void _GetTrustLevel__ctor_mEBC3CD6FF5069E35916B2603809587F20DBAC27F (void);
// 0x000000C2 System.Int32 WinRT.Interop.IInspectableVftbl/_GetTrustLevel::Invoke(System.IntPtr,WinRT.TrustLevel*)
extern void _GetTrustLevel_Invoke_m61C4F3C6B1E37253AA6CF120FF72A82A5013DA4B (void);
// 0x000000C3 System.IAsyncResult WinRT.Interop.IInspectableVftbl/_GetTrustLevel::BeginInvoke(System.IntPtr,WinRT.TrustLevel*,System.AsyncCallback,System.Object)
extern void _GetTrustLevel_BeginInvoke_mB5E1DB180FC10F5B94FDD96C3DDEE15BE880BFCC (void);
// 0x000000C4 System.Int32 WinRT.Interop.IInspectableVftbl/_GetTrustLevel::EndInvoke(System.IAsyncResult)
extern void _GetTrustLevel_EndInvoke_m730CBCA9351A942865586EDEE5371A646F68580F (void);
// 0x000000C5 System.Void WinRT.Interop.IActivationFactoryVftbl/_ActivateInstance::.ctor(System.Object,System.IntPtr)
extern void _ActivateInstance__ctor_mF8806B7C77EF673468D808E5BC4D53BC19F4A77A (void);
// 0x000000C6 System.Int32 WinRT.Interop.IActivationFactoryVftbl/_ActivateInstance::Invoke(System.IntPtr,System.IntPtr*)
extern void _ActivateInstance_Invoke_mB08503B0ADF6DD6FEA50632E78A60FF4BF35D6A4 (void);
// 0x000000C7 System.IAsyncResult WinRT.Interop.IActivationFactoryVftbl/_ActivateInstance::BeginInvoke(System.IntPtr,System.IntPtr*,System.AsyncCallback,System.Object)
extern void _ActivateInstance_BeginInvoke_m27D683CB18AC085880F879D26EE91CCF210A0931 (void);
// 0x000000C8 System.Int32 WinRT.Interop.IActivationFactoryVftbl/_ActivateInstance::EndInvoke(System.IAsyncResult)
extern void _ActivateInstance_EndInvoke_m70C5C9470E079476EFC29CFDA01AC189B9E3AF00 (void);
// 0x000000C9 System.Boolean WinRT.Interop._Bool::op_Implicit(WinRT.Interop._Bool)
extern void _Bool_op_Implicit_m76F768EE291283874AA5CF28D78DE6949EF1A116 (void);
// 0x000000CA System.Void WinRT.Interop._get_PropertyAsInt::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsInt__ctor_mDD70D3194BACC86D5EF741EB608BBBAAD9711EAF (void);
// 0x000000CB System.Int32 WinRT.Interop._get_PropertyAsInt::Invoke(System.IntPtr,System.Int32*)
extern void _get_PropertyAsInt_Invoke_mA2CB9549CA91604196249944CC34B6F518C15E5F (void);
// 0x000000CC System.IAsyncResult WinRT.Interop._get_PropertyAsInt::BeginInvoke(System.IntPtr,System.Int32*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsInt_BeginInvoke_m5567B2037276D6EF16DE078443BBF5D71FE93633 (void);
// 0x000000CD System.Int32 WinRT.Interop._get_PropertyAsInt::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsInt_EndInvoke_m7FD377AAB498DC759FD0A62C4DB52D85184F9188 (void);
// 0x000000CE System.Void WinRT.Interop._get_PropertyAsUInt::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsUInt__ctor_mA6B3292830A2E51BBDCDD0BBBB0357BA2AF15835 (void);
// 0x000000CF System.Int32 WinRT.Interop._get_PropertyAsUInt::Invoke(System.IntPtr,System.UInt32*)
extern void _get_PropertyAsUInt_Invoke_mEF9E93E0B6F5D5280579B4723C7BC2BA514E4800 (void);
// 0x000000D0 System.IAsyncResult WinRT.Interop._get_PropertyAsUInt::BeginInvoke(System.IntPtr,System.UInt32*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsUInt_BeginInvoke_mB17791AE904E0072103D2D17470D7290C59C1172 (void);
// 0x000000D1 System.Int32 WinRT.Interop._get_PropertyAsUInt::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsUInt_EndInvoke_mE42F919AF4C9713D29237702339BEA959BB09E8D (void);
// 0x000000D2 System.Void WinRT.Interop._get_PropertyAsBool::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsBool__ctor_m3EE433788D58015F949EC3C8696453CE73EDAE97 (void);
// 0x000000D3 System.Int32 WinRT.Interop._get_PropertyAsBool::Invoke(System.IntPtr,WinRT.Interop._Bool*)
extern void _get_PropertyAsBool_Invoke_m3C98EEE4D236E038ADB9D16A9D7438FE533C38FA (void);
// 0x000000D4 System.IAsyncResult WinRT.Interop._get_PropertyAsBool::BeginInvoke(System.IntPtr,WinRT.Interop._Bool*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsBool_BeginInvoke_m0114D04318E0BB40B8274FD6FD6FFE645A937DEB (void);
// 0x000000D5 System.Int32 WinRT.Interop._get_PropertyAsBool::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsBool_EndInvoke_m517BF03B243B7B394A0815434B41DB7DE9468F47 (void);
// 0x000000D6 System.Void WinRT.Interop._get_PropertyAsFloat::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsFloat__ctor_m5478F78469497B7BDA57B46538028868C43FFBF5 (void);
// 0x000000D7 System.Int32 WinRT.Interop._get_PropertyAsFloat::Invoke(System.IntPtr,System.Single*)
extern void _get_PropertyAsFloat_Invoke_m2C75BDB217D8FCDE434CAE0E194CEECA594891CF (void);
// 0x000000D8 System.IAsyncResult WinRT.Interop._get_PropertyAsFloat::BeginInvoke(System.IntPtr,System.Single*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsFloat_BeginInvoke_m46C046675813C3FA76CB8B3E9E2386DBB7BF1E8B (void);
// 0x000000D9 System.Int32 WinRT.Interop._get_PropertyAsFloat::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsFloat_EndInvoke_m4DDD95F6CFBA5C2D8010DBF62D8E7E803D0893CB (void);
// 0x000000DA System.Void WinRT.Interop._get_PropertyAsObject::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsObject__ctor_mD9A46F1655DCBF7AB86C44451B0C6D4BC06677F2 (void);
// 0x000000DB System.Int32 WinRT.Interop._get_PropertyAsObject::Invoke(System.IntPtr,System.IntPtr*)
extern void _get_PropertyAsObject_Invoke_m1917D8F5BA5A86416C04A0E0FAD38CE827503EA9 (void);
// 0x000000DC System.IAsyncResult WinRT.Interop._get_PropertyAsObject::BeginInvoke(System.IntPtr,System.IntPtr*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsObject_BeginInvoke_m4891144DE614B249C650221139044B2C193F54AA (void);
// 0x000000DD System.Int32 WinRT.Interop._get_PropertyAsObject::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsObject_EndInvoke_mE155EE6CE7E2B3F0EF7ED5218142A99B3D523BCD (void);
// 0x000000DE System.Void WinRT.Interop._put_PropertyAsObject::.ctor(System.Object,System.IntPtr)
extern void _put_PropertyAsObject__ctor_mB695938A9D887B64FA231705933D8DE7173B0177 (void);
// 0x000000DF System.Int32 WinRT.Interop._put_PropertyAsObject::Invoke(System.IntPtr,System.IntPtr)
extern void _put_PropertyAsObject_Invoke_mDF1D89E8D5364CCF513E0044AFBD071ED450DD0B (void);
// 0x000000E0 System.IAsyncResult WinRT.Interop._put_PropertyAsObject::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern void _put_PropertyAsObject_BeginInvoke_mC041F0BDE7B798F61293772E2597EE1BD278A1D9 (void);
// 0x000000E1 System.Int32 WinRT.Interop._put_PropertyAsObject::EndInvoke(System.IAsyncResult)
extern void _put_PropertyAsObject_EndInvoke_mCB62434060807BDDC648A4694556AFF62D738776 (void);
// 0x000000E2 System.Void WinRT.Interop._get_PropertyAsEnum::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsEnum__ctor_mA694C57389947CD3E2511FDB9525054C5A38E9FE (void);
// 0x000000E3 System.Int32 WinRT.Interop._get_PropertyAsEnum::Invoke(System.IntPtr,System.Int32*)
extern void _get_PropertyAsEnum_Invoke_mA3E0555F3A72CAF26FE3CB281262E1333073A3AA (void);
// 0x000000E4 System.IAsyncResult WinRT.Interop._get_PropertyAsEnum::BeginInvoke(System.IntPtr,System.Int32*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsEnum_BeginInvoke_m578435201CB3224D9AFF9C4C98B6575F9646B44D (void);
// 0x000000E5 System.Int32 WinRT.Interop._get_PropertyAsEnum::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsEnum_EndInvoke_mA3AFF6E24299B0D14C5B119CA94E04E3CE3521EA (void);
// 0x000000E6 System.Void WinRT.Interop._get_PropertyAsGuid::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsGuid__ctor_m73E55A34DCB8AB768A8C1C537C8F21AE47F91C89 (void);
// 0x000000E7 System.Int32 WinRT.Interop._get_PropertyAsGuid::Invoke(System.IntPtr,System.Guid*)
extern void _get_PropertyAsGuid_Invoke_mC7D08CDB9AE9B1464D8BB683A07D1E85B36E987C (void);
// 0x000000E8 System.IAsyncResult WinRT.Interop._get_PropertyAsGuid::BeginInvoke(System.IntPtr,System.Guid*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsGuid_BeginInvoke_m617C12B5DEB0A5C3696C8D9DA6A3D51E98180F23 (void);
// 0x000000E9 System.Int32 WinRT.Interop._get_PropertyAsGuid::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsGuid_EndInvoke_m1DD54E4F9AFFEE6DAE85E218C3F9F2580DC2C362 (void);
// 0x000000EA System.Void WinRT.Interop._get_PropertyAsDateTime::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsDateTime__ctor_m725E9CDDC0C23D119E125DC4D0909DA4FE234727 (void);
// 0x000000EB System.Int32 WinRT.Interop._get_PropertyAsDateTime::Invoke(System.IntPtr,WinRT.Interop._DateTime*)
extern void _get_PropertyAsDateTime_Invoke_mC79711A23B5E8DC84E46390DF883E05126624A31 (void);
// 0x000000EC System.IAsyncResult WinRT.Interop._get_PropertyAsDateTime::BeginInvoke(System.IntPtr,WinRT.Interop._DateTime*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsDateTime_BeginInvoke_mFF637BDDB82458912F1F6AF8EC6037CA7E51C8AD (void);
// 0x000000ED System.Int32 WinRT.Interop._get_PropertyAsDateTime::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsDateTime_EndInvoke_m386A4E9BC2AEC626CB2685F8AAEDF53EBCD74355 (void);
// 0x000000EE System.Void WinRT.Interop._get_PropertyAsTimeSpan::.ctor(System.Object,System.IntPtr)
extern void _get_PropertyAsTimeSpan__ctor_m5B4FC792F5263385DF8EB01379107208FDA62F84 (void);
// 0x000000EF System.Int32 WinRT.Interop._get_PropertyAsTimeSpan::Invoke(System.IntPtr,WinRT.Interop._TimeSpan*)
extern void _get_PropertyAsTimeSpan_Invoke_m55E6AFDF1EF562CF51E0690D80CCEFFBAB135D96 (void);
// 0x000000F0 System.IAsyncResult WinRT.Interop._get_PropertyAsTimeSpan::BeginInvoke(System.IntPtr,WinRT.Interop._TimeSpan*,System.AsyncCallback,System.Object)
extern void _get_PropertyAsTimeSpan_BeginInvoke_m8E875D2353F52EC9F9B94F281E28E0984A43FCFF (void);
// 0x000000F1 System.Int32 WinRT.Interop._get_PropertyAsTimeSpan::EndInvoke(System.IAsyncResult)
extern void _get_PropertyAsTimeSpan_EndInvoke_m3D258C5DC0E2BCFE0AE8B658E3286E23E57F9712 (void);
// 0x000000F2 System.Void WinRT.Interop._add_EventHandler::.ctor(System.Object,System.IntPtr)
extern void _add_EventHandler__ctor_mCAA996DED22A25CFE3245E3D86319EDA93B9556A (void);
// 0x000000F3 System.Int32 WinRT.Interop._add_EventHandler::Invoke(System.IntPtr,System.IntPtr,WinRT.Interop.EventRegistrationToken*)
extern void _add_EventHandler_Invoke_mD4BDAA8CEF55F4ECC1E6F8D5110EFCC50561B48D (void);
// 0x000000F4 System.IAsyncResult WinRT.Interop._add_EventHandler::BeginInvoke(System.IntPtr,System.IntPtr,WinRT.Interop.EventRegistrationToken*,System.AsyncCallback,System.Object)
extern void _add_EventHandler_BeginInvoke_mEE2A5DDFBE9F30FCD1156AEC48B32D81BE4553E4 (void);
// 0x000000F5 System.Int32 WinRT.Interop._add_EventHandler::EndInvoke(System.IAsyncResult)
extern void _add_EventHandler_EndInvoke_m958E5F83A23CDAEA8F23F3E70AA4E005B50CE633 (void);
// 0x000000F6 System.Void WinRT.Interop._remove_EventHandler::.ctor(System.Object,System.IntPtr)
extern void _remove_EventHandler__ctor_m410B32951AC0477566CCFE04FB07D4E223A690E6 (void);
// 0x000000F7 System.Int32 WinRT.Interop._remove_EventHandler::Invoke(System.IntPtr,WinRT.Interop.EventRegistrationToken)
extern void _remove_EventHandler_Invoke_m3D8F86DED311BDBB555A31FCEFE25EB9AE53ACEC (void);
// 0x000000F8 System.IAsyncResult WinRT.Interop._remove_EventHandler::BeginInvoke(System.IntPtr,WinRT.Interop.EventRegistrationToken,System.AsyncCallback,System.Object)
extern void _remove_EventHandler_BeginInvoke_mDCC3991FEED00E07E9AE187DD3AED334737055EA (void);
// 0x000000F9 System.Int32 WinRT.Interop._remove_EventHandler::EndInvoke(System.IAsyncResult)
extern void _remove_EventHandler_EndInvoke_m880B2E61D4FF2EB2338E6F41AEDF28B116CA1B4B (void);
// 0x000000FA System.Void WinRT.Interop._method0::.ctor(System.Object,System.IntPtr)
extern void _method0__ctor_mF45D4765F0CC1C4AB0046CE7B9047B9524AA8A58 (void);
// 0x000000FB System.Int32 WinRT.Interop._method0::Invoke(System.IntPtr)
extern void _method0_Invoke_m5BA392D9F9112B38345B2A6FEA907A62EA2ED602 (void);
// 0x000000FC System.IAsyncResult WinRT.Interop._method0::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void _method0_BeginInvoke_m0D76CD50246644B8D98FD95E1C04E3A7B77221F7 (void);
// 0x000000FD System.Int32 WinRT.Interop._method0::EndInvoke(System.IAsyncResult)
extern void _method0_EndInvoke_m8FA7FD1243B8E7F1060422BF60D22ECC13EBC5B6 (void);
// 0x000000FE System.Void WinRT.Interop.IDelegate2_Obj_Obj::.ctor(System.Object,System.IntPtr)
extern void IDelegate2_Obj_Obj__ctor_m0D3CF7401ED981769154DC7772D9C4FC163C8501 (void);
// 0x000000FF System.Int32 WinRT.Interop.IDelegate2_Obj_Obj::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern void IDelegate2_Obj_Obj_Invoke_m585BD2DE510F03C1EC0C7217A67C5B2EDB864D8A (void);
// 0x00000100 System.IAsyncResult WinRT.Interop.IDelegate2_Obj_Obj::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern void IDelegate2_Obj_Obj_BeginInvoke_m4A18C2480FDA5F71E07F31026E5AD42254B193A2 (void);
// 0x00000101 System.Int32 WinRT.Interop.IDelegate2_Obj_Obj::EndInvoke(System.IAsyncResult)
extern void IDelegate2_Obj_Obj_EndInvoke_mAC234DFF0459C5A7A3BCE892FEBBA0CDBD105D96 (void);
// 0x00000102 System.Void WinRT.Interop.IDelegate2_Obj_Enum::.ctor(System.Object,System.IntPtr)
extern void IDelegate2_Obj_Enum__ctor_m13EDE19A4F89CA35FA9B17EFF088ED5B043F96B1 (void);
// 0x00000103 System.Int32 WinRT.Interop.IDelegate2_Obj_Enum::Invoke(System.IntPtr,System.IntPtr,System.Int32)
extern void IDelegate2_Obj_Enum_Invoke_m911F5B11055AB8307E957BC41974A893D2F63A9E (void);
// 0x00000104 System.IAsyncResult WinRT.Interop.IDelegate2_Obj_Enum::BeginInvoke(System.IntPtr,System.IntPtr,System.Int32,System.AsyncCallback,System.Object)
extern void IDelegate2_Obj_Enum_BeginInvoke_m16FDE209FE6E1EC9DCD8C16464D6E13F34308A7F (void);
// 0x00000105 System.Int32 WinRT.Interop.IDelegate2_Obj_Enum::EndInvoke(System.IAsyncResult)
extern void IDelegate2_Obj_Enum_EndInvoke_m1884125D339924E560796FCBF0E4437A325CDFA6 (void);
// 0x00000106 System.Void WinRT.Interop.DelegateProperty`1::.ctor(System.Guid,WinRT.IObjectReference,WinRT.Interop._get_PropertyAsObject,WinRT.Interop._put_PropertyAsObject,System.IntPtr)
// 0x00000107 System.Void WinRT.Interop.DelegateProperty`1::set_Value(T)
// 0x00000108 WinRT.Interop.IAsyncInfo WinRT.Interop.IAsyncInfo::op_Implicit(WinRT.IObjectReference)
extern void IAsyncInfo_op_Implicit_m16C479275A56E345498B0DED78B3D94675617444 (void);
// 0x00000109 WinRT.Interop.IAsyncInfo WinRT.Interop.IAsyncInfo::op_Implicit(WinRT.ObjectReference`1<WinRT.Interop.IAsyncInfo/Vftbl>)
extern void IAsyncInfo_op_Implicit_mDF153DE0BB13D327BD541D5BD8F0A03484C7444E (void);
// 0x0000010A System.Void WinRT.Interop.IAsyncInfo::.ctor(WinRT.ObjectReference`1<WinRT.Interop.IAsyncInfo/Vftbl>)
extern void IAsyncInfo__ctor_m5F3E8229900F0999503495D7B0C5CB37C42CE143 (void);
// 0x0000010B System.Exception WinRT.Interop.IAsyncInfo::get_ErrorCode()
extern void IAsyncInfo_get_ErrorCode_m1C25DABFC9FDBBC4446E9583206E7DBAE3DD500A (void);
// 0x0000010C System.Void WinRT.Interop.IAsyncInfo::Cancel()
extern void IAsyncInfo_Cancel_m09826943CD94AA1DBF8DD7E1D83B41415F2A7A5F (void);
// 0x0000010D System.Void WinRT.Interop.IAsyncOperation::.ctor(System.Guid,WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>)
extern void IAsyncOperation__ctor_m257FF1710789A572935C52FCAC888CC02C496029 (void);
// 0x0000010E System.Int32 WinRT.Interop.IAsyncOperation::_OnCompleted(System.IntPtr,System.IntPtr,System.Int32)
extern void IAsyncOperation__OnCompleted_m20528AE352B798EDDBFD33AA9064E6BFF7EB60C3 (void);
// 0x0000010F System.Void WinRT.Interop.IAsyncOperation::set_Completed(WinRT.Interop.IAsyncOperation/CompletedHandler)
extern void IAsyncOperation_set_Completed_m662E47E70A763D519B9948DDEFB2F478E5EF216D (void);
// 0x00000110 System.Void WinRT.Interop.IAsyncOperation::.cctor()
extern void IAsyncOperation__cctor_m6CAD2C06113F8204912825C8686EFA489DABFB92 (void);
// 0x00000111 System.Void WinRT.Interop.IAsyncOperation/CompletedHandler::.ctor(System.Object,System.IntPtr)
extern void CompletedHandler__ctor_mADCC48FAE51187931A2EB33CBA580F8C61E33428 (void);
// 0x00000112 System.Void WinRT.Interop.IAsyncOperation/CompletedHandler::Invoke(WinRT.AsyncStatus)
extern void CompletedHandler_Invoke_m3390A5EC8FBAEDB455755F72811899725EFFF0E6 (void);
// 0x00000113 System.IAsyncResult WinRT.Interop.IAsyncOperation/CompletedHandler::BeginInvoke(WinRT.AsyncStatus,System.AsyncCallback,System.Object)
extern void CompletedHandler_BeginInvoke_m87C75727ED4E2F6A162ED4294483FC833493F3E6 (void);
// 0x00000114 System.Void WinRT.Interop.IAsyncOperation/CompletedHandler::EndInvoke(System.IAsyncResult)
extern void CompletedHandler_EndInvoke_m3D066A294908D3068444F85F293DF7010CAEA359 (void);
// 0x00000115 System.Void WinRT.Interop.IAsyncOperation/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mE7DE39605653458D324B60FB3E71310EBFDA3398 (void);
// 0x00000116 System.Void WinRT.Interop.IAsyncOperation/<>c__DisplayClass4_0::<_OnCompleted>b__0(WinRT.Interop.IAsyncOperation/CompletedHandler)
extern void U3CU3Ec__DisplayClass4_0_U3C_OnCompletedU3Eb__0_m4BEE91D6307A33E33FB893A4AC781EE855E42FDF (void);
// 0x00000117 System.Void WinRT.Interop._IAsyncOperation`2::.ctor(System.Guid,WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>)
// 0x00000118 System.Void WinRT.Interop._IAsyncOperation`2::OnCompleted(System.Threading.Tasks.TaskCompletionSource`1<TResult>)
// 0x00000119 TTask WinRT.Interop._IAsyncOperation`2::AsTask()
// 0x0000011A TTask WinRT.Interop._IAsyncOperation`2::AsTask(System.Threading.CancellationToken)
// 0x0000011B System.Void WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::.ctor()
// 0x0000011C System.Void WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::<AsTask>b__0(WinRT.AsyncStatus)
// 0x0000011D System.Void WinRT.Interop._IAsyncOperation`2/<>c__DisplayClass3_0::<AsTask>b__1()
// 0x0000011E System.Void WinRT.Interop.IAsyncOperation`2::.ctor(WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>)
// 0x0000011F System.Void WinRT.Interop.IAsyncOperation`2::OnCompleted(System.Threading.Tasks.TaskCompletionSource`1<T>)
// 0x00000120 T WinRT.Interop.IAsyncOperation`2::GetResults()
// 0x00000121 System.Void WinRT.Interop.IAsyncOperation_Enum`2::.ctor(WinRT.ObjectReference`1<WinRT.Interop.IAsyncOperation/Vftbl>,System.Func`2<System.Int32,T>)
// 0x00000122 T WinRT.Interop.IAsyncOperation_Enum`2::GetResults()
// 0x00000123 Microsoft.MixedReality.QR.IQRCode Microsoft.MixedReality.QR.IQRCode::op_Implicit(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCode/Vftbl>)
extern void IQRCode_op_Implicit_mC303682F5AEBAB4467BCE37E49F493A5810D4CD6 (void);
// 0x00000124 Microsoft.MixedReality.QR.QRCode Microsoft.MixedReality.QR.IQRCode::op_Implicit(Microsoft.MixedReality.QR.IQRCode)
extern void IQRCode_op_Implicit_mD02E163196243DC5350493515129A1A7CA8610F7 (void);
// 0x00000125 System.Void Microsoft.MixedReality.QR.IQRCode::.ctor(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCode/Vftbl>)
extern void IQRCode__ctor_mE6045DB0E12EEBFB093D6C844BBB44078DA893A9 (void);
// 0x00000126 System.Guid Microsoft.MixedReality.QR.IQRCode::get_Id()
extern void IQRCode_get_Id_m2D3C9AA61F411F3343E815BCF3303D47DE66A911 (void);
// 0x00000127 System.Guid Microsoft.MixedReality.QR.IQRCode::get_SpatialGraphNodeId()
extern void IQRCode_get_SpatialGraphNodeId_m76D97656F7E3666EB37178C846F5201F975CF2A3 (void);
// 0x00000128 Microsoft.MixedReality.QR.QRVersion Microsoft.MixedReality.QR.IQRCode::get_Version()
extern void IQRCode_get_Version_mE034619125C1C0426C5095C97605C810042476F1 (void);
// 0x00000129 System.Single Microsoft.MixedReality.QR.IQRCode::get_PhysicalSideLength()
extern void IQRCode_get_PhysicalSideLength_mEF5BB92B87413DBB71E6AB10588C454504639A19 (void);
// 0x0000012A System.String Microsoft.MixedReality.QR.IQRCode::get_Data()
extern void IQRCode_get_Data_m3E2598ECE88F0CD99EA497902A20702DC8501ED0 (void);
// 0x0000012B System.TimeSpan Microsoft.MixedReality.QR.IQRCode::get_SystemRelativeLastDetectedTime()
extern void IQRCode_get_SystemRelativeLastDetectedTime_m218B0E8028C232BFA482C610D0FF00076CEF185D (void);
// 0x0000012C System.DateTimeOffset Microsoft.MixedReality.QR.IQRCode::get_LastDetectedTime()
extern void IQRCode_get_LastDetectedTime_m0B0D02A7BCB4D86FFF2AD848B1BF3CBAA215FF91 (void);
// 0x0000012D System.Void Microsoft.MixedReality.QR.IQRCode/Vftbl/_GetRawData::.ctor(System.Object,System.IntPtr)
extern void _GetRawData__ctor_mC95057AB611E0A11BB91681331B3532D550DF778 (void);
// 0x0000012E System.Int32 Microsoft.MixedReality.QR.IQRCode/Vftbl/_GetRawData::Invoke(System.IntPtr,System.UInt32,System.Byte*)
extern void _GetRawData_Invoke_m0028AC4CB8B33E72445A870B584257A4A7360053 (void);
// 0x0000012F System.IAsyncResult Microsoft.MixedReality.QR.IQRCode/Vftbl/_GetRawData::BeginInvoke(System.IntPtr,System.UInt32,System.Byte*,System.AsyncCallback,System.Object)
extern void _GetRawData_BeginInvoke_m2A2250655B4A6A9621F06DD4EE6D56C86BE4472F (void);
// 0x00000130 System.Int32 Microsoft.MixedReality.QR.IQRCode/Vftbl/_GetRawData::EndInvoke(System.IAsyncResult)
extern void _GetRawData_EndInvoke_mEB2419F85E526F1BF7E44408A0EAB27FCB61AE24 (void);
// 0x00000131 System.Void Microsoft.MixedReality.QR.QRCode::.ctor(Microsoft.MixedReality.QR.IQRCode)
extern void QRCode__ctor_mDA586215D17042FEA4F256EEACD5D8B5F31AA788 (void);
// 0x00000132 System.Guid Microsoft.MixedReality.QR.QRCode::get_Id()
extern void QRCode_get_Id_mE3FB5E809784AD1A16D3C9792089E36BF3EDA4EB (void);
// 0x00000133 System.Guid Microsoft.MixedReality.QR.QRCode::get_SpatialGraphNodeId()
extern void QRCode_get_SpatialGraphNodeId_m0C034B923BE48C3396269023364DEF35A94D388C (void);
// 0x00000134 Microsoft.MixedReality.QR.QRVersion Microsoft.MixedReality.QR.QRCode::get_Version()
extern void QRCode_get_Version_m720D2408C3EDBA82EC2C281ECCE2E48B9562C00C (void);
// 0x00000135 System.Single Microsoft.MixedReality.QR.QRCode::get_PhysicalSideLength()
extern void QRCode_get_PhysicalSideLength_mC7959DEEBE81AFB20E5BFD03851C7E09035B7E41 (void);
// 0x00000136 System.String Microsoft.MixedReality.QR.QRCode::get_Data()
extern void QRCode_get_Data_m1FC66FDD0F49E9F9D918648349318AC1E1986C50 (void);
// 0x00000137 System.TimeSpan Microsoft.MixedReality.QR.QRCode::get_SystemRelativeLastDetectedTime()
extern void QRCode_get_SystemRelativeLastDetectedTime_m0A1D9CBC227CEFC4D9622908C0F6A068EEB3EF8C (void);
// 0x00000138 System.DateTimeOffset Microsoft.MixedReality.QR.QRCode::get_LastDetectedTime()
extern void QRCode_get_LastDetectedTime_mFDA34B2DA60810867A81753D8E11C18AFC7B891A (void);
// 0x00000139 Microsoft.MixedReality.QR.IQRCodeAddedEventArgs Microsoft.MixedReality.QR.IQRCodeAddedEventArgs::op_Implicit(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeAddedEventArgs/Vftbl>)
extern void IQRCodeAddedEventArgs_op_Implicit_mF7FFC524B78EB499F15A973B87B5562A4F940B54 (void);
// 0x0000013A System.Void Microsoft.MixedReality.QR.IQRCodeAddedEventArgs::.ctor(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeAddedEventArgs/Vftbl>)
extern void IQRCodeAddedEventArgs__ctor_mFCBD2EB66AE675E9AF66CC45DCE4198E7CFD7D35 (void);
// 0x0000013B Microsoft.MixedReality.QR.IQRCode Microsoft.MixedReality.QR.IQRCodeAddedEventArgs::get_Code()
extern void IQRCodeAddedEventArgs_get_Code_m261A030AF42A1C737992DD410C75DF0CBBF24880 (void);
// 0x0000013C System.Void Microsoft.MixedReality.QR.QRCodeAddedEventArgs::.ctor(Microsoft.MixedReality.QR.IQRCodeAddedEventArgs)
extern void QRCodeAddedEventArgs__ctor_m23959E9410B31231059AF5F84A1496454402FE2C (void);
// 0x0000013D Microsoft.MixedReality.QR.QRCode Microsoft.MixedReality.QR.QRCodeAddedEventArgs::get_Code()
extern void QRCodeAddedEventArgs_get_Code_mE9EABF426E43F7F28BD0A029DE2CABD2EB022A98 (void);
// 0x0000013E Microsoft.MixedReality.QR.IQRCodeUpdatedEventArgs Microsoft.MixedReality.QR.IQRCodeUpdatedEventArgs::op_Implicit(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeUpdatedEventArgs/Vftbl>)
extern void IQRCodeUpdatedEventArgs_op_Implicit_mDC1DA085E3144034987F9B9F395316F1611BAB04 (void);
// 0x0000013F System.Void Microsoft.MixedReality.QR.IQRCodeUpdatedEventArgs::.ctor(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeUpdatedEventArgs/Vftbl>)
extern void IQRCodeUpdatedEventArgs__ctor_mA50C0647EFDACEABEE10CB72C83AE91521B64878 (void);
// 0x00000140 Microsoft.MixedReality.QR.IQRCode Microsoft.MixedReality.QR.IQRCodeUpdatedEventArgs::get_Code()
extern void IQRCodeUpdatedEventArgs_get_Code_m9E0592C6FFC43D307FA4A8F46BC846FA846E1D97 (void);
// 0x00000141 System.Void Microsoft.MixedReality.QR.QRCodeUpdatedEventArgs::.ctor(Microsoft.MixedReality.QR.IQRCodeUpdatedEventArgs)
extern void QRCodeUpdatedEventArgs__ctor_m2ACB70F5CB45FBC028B3794D603F3A1F99547781 (void);
// 0x00000142 Microsoft.MixedReality.QR.QRCode Microsoft.MixedReality.QR.QRCodeUpdatedEventArgs::get_Code()
extern void QRCodeUpdatedEventArgs_get_Code_m768700916CA1AC3F786B4B6AD6FE2F7D2A4CFA6E (void);
// 0x00000143 Microsoft.MixedReality.QR.IQRCodeRemovedEventArgs Microsoft.MixedReality.QR.IQRCodeRemovedEventArgs::op_Implicit(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeRemovedEventArgs/Vftbl>)
extern void IQRCodeRemovedEventArgs_op_Implicit_m09ACBC4DE6D7E030D1BDF7A46335795A07264830 (void);
// 0x00000144 System.Void Microsoft.MixedReality.QR.IQRCodeRemovedEventArgs::.ctor(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeRemovedEventArgs/Vftbl>)
extern void IQRCodeRemovedEventArgs__ctor_mF00B77CF8CC0DFF8861F180A3F65625528B2D744 (void);
// 0x00000145 Microsoft.MixedReality.QR.IQRCode Microsoft.MixedReality.QR.IQRCodeRemovedEventArgs::get_Code()
extern void IQRCodeRemovedEventArgs_get_Code_mF5D3C8EAC6FF98FE702A13410DC19EF6821D0878 (void);
// 0x00000146 System.Void Microsoft.MixedReality.QR.QRCodeRemovedEventArgs::.ctor(Microsoft.MixedReality.QR.IQRCodeRemovedEventArgs)
extern void QRCodeRemovedEventArgs__ctor_m90DFD3C5166DACB7881CFC454F5A4AC02A725ECC (void);
// 0x00000147 Microsoft.MixedReality.QR.QRCode Microsoft.MixedReality.QR.QRCodeRemovedEventArgs::get_Code()
extern void QRCodeRemovedEventArgs_get_Code_m7C505245A79F9A072B6134CF0971E4D444B08E94 (void);
// 0x00000148 Microsoft.MixedReality.QR.IQRCodeWatcher Microsoft.MixedReality.QR.IQRCodeWatcher::op_Implicit(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeWatcher/Vftbl>)
extern void IQRCodeWatcher_op_Implicit_m9B117FF748971AE401014678C515CC439BDD106C (void);
// 0x00000149 System.Void Microsoft.MixedReality.QR.IQRCodeWatcher::.ctor(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeWatcher/Vftbl>)
extern void IQRCodeWatcher__ctor_m37BC9B21B5AD1867C2A8440EF08903284829281F (void);
// 0x0000014A System.Void Microsoft.MixedReality.QR.IQRCodeWatcher::Start()
extern void IQRCodeWatcher_Start_m4C5AC51B29DC53D2A4A1546E63896179ED66A396 (void);
// 0x0000014B System.Void Microsoft.MixedReality.QR.IQRCodeWatcher::Stop()
extern void IQRCodeWatcher_Stop_m4E45FEABF05F18CE4C06A6E852B52EA99F9B67FF (void);
// 0x0000014C System.Void Microsoft.MixedReality.QR.IQRCodeWatcher/<>c::.cctor()
extern void U3CU3Ec__cctor_m9725FEA2D1F1EED2E01193C1B06D8CE17EC5C477 (void);
// 0x0000014D System.Void Microsoft.MixedReality.QR.IQRCodeWatcher/<>c::.ctor()
extern void U3CU3Ec__ctor_m130720570E2D4FAAF66D5FD1925C5F055ACDF9DD (void);
// 0x0000014E Microsoft.MixedReality.QR.QRCodeAddedEventArgs Microsoft.MixedReality.QR.IQRCodeWatcher/<>c::<.ctor>b__5_0(System.IntPtr)
extern void U3CU3Ec_U3C_ctorU3Eb__5_0_m2853C1F50981B6B7B0A012041EA9751D91CDD62B (void);
// 0x0000014F Microsoft.MixedReality.QR.QRCodeUpdatedEventArgs Microsoft.MixedReality.QR.IQRCodeWatcher/<>c::<.ctor>b__5_1(System.IntPtr)
extern void U3CU3Ec_U3C_ctorU3Eb__5_1_mFA8C3690728543E8D2FED51F276E3B957021614C (void);
// 0x00000150 Microsoft.MixedReality.QR.QRCodeRemovedEventArgs Microsoft.MixedReality.QR.IQRCodeWatcher/<>c::<.ctor>b__5_2(System.IntPtr)
extern void U3CU3Ec_U3C_ctorU3Eb__5_2_m36DB53B9D6A42ABB19076134730CBE911A20CAED (void);
// 0x00000151 System.Object Microsoft.MixedReality.QR.IQRCodeWatcher/<>c::<.ctor>b__5_3(System.IntPtr)
extern void U3CU3Ec_U3C_ctorU3Eb__5_3_m197CDFD32C58CBFA33FC80E260E0FD2E2DC11FB7 (void);
// 0x00000152 System.Void Microsoft.MixedReality.QR.IQRCodeWatcherStatics::.ctor(WinRT.ObjectReference`1<Microsoft.MixedReality.QR.IQRCodeWatcherStatics/Vftbl>)
extern void IQRCodeWatcherStatics__ctor_mE76E870AC41F6FA207116C82F1EF1BB44B752943 (void);
// 0x00000153 System.Boolean Microsoft.MixedReality.QR.IQRCodeWatcherStatics::IsSupported()
extern void IQRCodeWatcherStatics_IsSupported_m942EB3628449DA3DB35D7193437B3FFD71BE4670 (void);
// 0x00000154 System.Threading.Tasks.Task`1<Microsoft.MixedReality.QR.QRCodeWatcherAccessStatus> Microsoft.MixedReality.QR.IQRCodeWatcherStatics::RequestAccessAsync()
extern void IQRCodeWatcherStatics_RequestAccessAsync_m490DAD715199242DA175A65E13B38BEDE0B0EA5A (void);
// 0x00000155 System.Void Microsoft.MixedReality.QR.IQRCodeWatcherStatics/Vftbl/_RequestAccessAsync::.ctor(System.Object,System.IntPtr)
extern void _RequestAccessAsync__ctor_m533C80BD0BB82AC1C49EC87479811F4FF0919B1A (void);
// 0x00000156 System.Int32 Microsoft.MixedReality.QR.IQRCodeWatcherStatics/Vftbl/_RequestAccessAsync::Invoke(System.IntPtr,System.IntPtr*)
extern void _RequestAccessAsync_Invoke_mCB5B76596DAD516FBAEDF9CCE615071E0C5D7B2B (void);
// 0x00000157 System.IAsyncResult Microsoft.MixedReality.QR.IQRCodeWatcherStatics/Vftbl/_RequestAccessAsync::BeginInvoke(System.IntPtr,System.IntPtr*,System.AsyncCallback,System.Object)
extern void _RequestAccessAsync_BeginInvoke_m45E12D5B1E8B24A5DF7D8933F0105E5EDE121A5E (void);
// 0x00000158 System.Int32 Microsoft.MixedReality.QR.IQRCodeWatcherStatics/Vftbl/_RequestAccessAsync::EndInvoke(System.IAsyncResult)
extern void _RequestAccessAsync_EndInvoke_m4A4A44C8046EABFBB7B5D716C56AD18016A1A150 (void);
// 0x00000159 System.Void Microsoft.MixedReality.QR.IQRCodeWatcherStatics/<>c::.cctor()
extern void U3CU3Ec__cctor_m3493D132B190D872A69D0225E1B618C0483D6CDF (void);
// 0x0000015A System.Void Microsoft.MixedReality.QR.IQRCodeWatcherStatics/<>c::.ctor()
extern void U3CU3Ec__ctor_mA0A352B377891454345F2AF737437E14166B0FA1 (void);
// 0x0000015B Microsoft.MixedReality.QR.QRCodeWatcherAccessStatus Microsoft.MixedReality.QR.IQRCodeWatcherStatics/<>c::<RequestAccessAsync>b__6_0(System.Int32)
extern void U3CU3Ec_U3CRequestAccessAsyncU3Eb__6_0_m7EDD33D4947620D2B4AA9AA549C03993EB52A3CD (void);
// 0x0000015C System.Boolean Microsoft.MixedReality.QR.QRCodeWatcher::IsSupported()
extern void QRCodeWatcher_IsSupported_m9B155614ABBE5A10B5C79AB65A25678B7DC69E5A (void);
// 0x0000015D System.Threading.Tasks.Task`1<Microsoft.MixedReality.QR.QRCodeWatcherAccessStatus> Microsoft.MixedReality.QR.QRCodeWatcher::RequestAccessAsync()
extern void QRCodeWatcher_RequestAccessAsync_m9F39960B5369B59D81F9ECC8D11581A775CB2BDF (void);
// 0x0000015E System.Void Microsoft.MixedReality.QR.QRCodeWatcher::.ctor(Microsoft.MixedReality.QR.IQRCodeWatcher)
extern void QRCodeWatcher__ctor_mDBD69C29C79CAA338D54ACDB4FBA4B0F019A2BBD (void);
// 0x0000015F System.Void Microsoft.MixedReality.QR.QRCodeWatcher::.ctor()
extern void QRCodeWatcher__ctor_mB6A3F7902F8F8C24C9B2A380369D7D5113C4FF09 (void);
// 0x00000160 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::Start()
extern void QRCodeWatcher_Start_m032F0A1254BFA694CA0D9C2076B41427279FFDEF (void);
// 0x00000161 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::Stop()
extern void QRCodeWatcher_Stop_m1AF8DE0E90588672674A4826712BED22057E9842 (void);
// 0x00000162 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::add_Added(System.EventHandler`1<Microsoft.MixedReality.QR.QRCodeAddedEventArgs>)
extern void QRCodeWatcher_add_Added_m8EA1CC941860DB9FC7374F2FA915E2DA6EE07A40 (void);
// 0x00000163 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::remove_Added(System.EventHandler`1<Microsoft.MixedReality.QR.QRCodeAddedEventArgs>)
extern void QRCodeWatcher_remove_Added_m3150FFE5E8A235082A3D496ECE6F67391AC8A9EB (void);
// 0x00000164 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::add_Updated(System.EventHandler`1<Microsoft.MixedReality.QR.QRCodeUpdatedEventArgs>)
extern void QRCodeWatcher_add_Updated_mB77CE150EFB8DAC8BE2577024445FD957D709A25 (void);
// 0x00000165 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::remove_Updated(System.EventHandler`1<Microsoft.MixedReality.QR.QRCodeUpdatedEventArgs>)
extern void QRCodeWatcher_remove_Updated_m3FC6201AB7FF343BE1502FFB7B4F4724A5BCB221 (void);
// 0x00000166 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::add_Removed(System.EventHandler`1<Microsoft.MixedReality.QR.QRCodeRemovedEventArgs>)
extern void QRCodeWatcher_add_Removed_m7F660B5AF724FBF88A409F25578F95EB29100556 (void);
// 0x00000167 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::remove_Removed(System.EventHandler`1<Microsoft.MixedReality.QR.QRCodeRemovedEventArgs>)
extern void QRCodeWatcher_remove_Removed_m16384F8FCCE8A9C04B84B659DE97C51AD20896E5 (void);
// 0x00000168 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::add_EnumerationCompleted(System.EventHandler`1<System.Object>)
extern void QRCodeWatcher_add_EnumerationCompleted_mD126E6A8F90E20D89D05BB6834510EF8C0FCC89C (void);
// 0x00000169 System.Void Microsoft.MixedReality.QR.QRCodeWatcher::remove_EnumerationCompleted(System.EventHandler`1<System.Object>)
extern void QRCodeWatcher_remove_EnumerationCompleted_m3DCB61192F38A71BA1542689A69AD35BEF8ECEE5 (void);
// 0x0000016A System.Void Microsoft.MixedReality.QR.QRCodeWatcher::.cctor()
extern void QRCodeWatcher__cctor_m6B0988DF823FB258F2F98B96C3DC7F12D4F663A8 (void);
// 0x0000016B System.Void Microsoft.MixedReality.QR.QRCodeWatcher/Statics::.ctor()
extern void Statics__ctor_m9C94C85B4E194448E31E44BE08D41F9EBB64E21C (void);
static Il2CppMethodPointer s_methodPointers[363] = 
{
	EmbeddedAttribute__ctor_mB1D74745CAC4D3A311F24386C40E32BE690F6399,
	IsUnmanagedAttribute__ctor_m019D815CD79A489B614C483183236BE40AA3D07A,
	Platform__cctor_m4D61405C27FCE515FBC61BC396345E62F5E7BEE1,
	Platform_TryLoadLibraryExW_mB2820476A3A8B379EF7494738B2A588DE0C47602,
	Platform_FreeLibrary_mB40A97287729A7D9CA08F019D5E2B3FD2182BADE,
	NULL,
	NULL,
	Platform_CoIncrementMTAUsage_mAB298D3128173B52D499DB013F6E8E6862362B6A,
	Platform_CoDecrementMTAUsage_m5C6359F3746212DE040D315FB3E2F82277DFF742,
	Platform_RoGetActivationFactory_mAD5908D6ACB3F1C3D9DE7A8C442A39A8C198B7DC,
	Platform_WindowsCreateString_mF7B414388C91EA50DDE89FDA5E43637E78F81534,
	Platform_WindowsGetStringRawBuffer_m8E74BB4093C72287451E59A003A823DB3AE239FB,
	Platform_WindowsDeleteString_mEFE91DAD454136EFC6F52BD9444FF690ACF5342A,
	Platform_GetHRForLastWin32Error_m1FF0B1A7D52C63BFCA22F2058BDB629FF4D37443,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DotNETLinkage__LoadLibraryExW_m57265357C3DFDCF43B21CDDB46D61EDECC386EF7,
	DotNETLinkage__FreeLibrary_mE2615802B5349AA67A85E760E603CE5CA5EAAD5E,
	DotNETLinkage__GetProcAddress_m2C02C031E6FA25474D1420576FC0FE145A4CBE0B,
	DotNETLinkage__CoIncrementMTAUsage_m1150721B485961618B37E292796E46A972F4F928,
	DotNETLinkage__CoDecrementMTAUsage_mC6258F7559A7C6307F4ED78547F0C4D26EE0F0D1,
	DotNETLinkage__RoGetActivationFactory_mC56B28FEA0341E4BE794771B17DEF12D9B0407F7,
	DotNETLinkage__WindowsCreateString_m48005411B4C935BA857A2204AD7CFA39C4A1A99A,
	DotNETLinkage__WindowsGetStringRawBuffer_m00B5E990E8330854817F9CD72F93073CCE026DAF,
	DotNETLinkage__WindowsDeleteString_m67F959C95FF3766B4383B107D84B74FAD7FDD536,
	DotNETLinkage__GetHRForLastWin32Error_mA31EFCA52B9F624FCB75AAC06A35DBBF720FC217,
	DotNETLinkage_LoadLibraryExW_m9A367A0B52A833F9C0291AFC496C26F996C76427,
	DotNETLinkage_FreeLibrary_mC09C5A3F3659FC0E217530199A0F3AEDC706DE6B,
	DotNETLinkage_GetProcAddress_m7D524CC8A320B481AE548AE593A42BF26FF0E7D5,
	DotNETLinkage_CoIncrementMTAUsage_mE118702CCC99AE5C567185B089EF3C7365B4111E,
	DotNETLinkage_CoDecrementMTAUsage_mB6BFEF95BED2ADFD7B418C4901391FCDAA0022C1,
	DotNETLinkage_RoGetActivationFactory_mB6FC97CA6B844D391CAD990AE950977117CB819A,
	DotNETLinkage_WindowsCreateString_m843371AB0F6853FAF67FE9AD83C1FBC4CE8423EA,
	DotNETLinkage_WindowsGetStringRawBuffer_m1223DC994D7EB97063CC8F6DAB77D8DA0242B268,
	DotNETLinkage_WindowsDeleteString_mCC1A08CAA3C34FCF98B72BAD78E48A5A38F1D8DD,
	DotNETLinkage__ctor_mB074F3B340B74F9367FA09CA0F1F899C5A483466,
	IL2CPPLinkage__LoadLibraryExW_mEE765D0163357934C9FFDF577956814601A869C4,
	IL2CPPLinkage__FreeLibrary_m4F8F399A4BD1CA05F5B616515F469E56C6EFCC4D,
	IL2CPPLinkage__GetProcAddress_m5878AA4B9AAF72AD1773F6CDF49D2D43D023BDC9,
	IL2CPPLinkage__CoIncrementMTAUsage_mD9780DD9222D198C9C1E5D718689EC44889FFEDD,
	IL2CPPLinkage__CoDecrementMTAUsage_mA6085E341D402E5C841239F5E8729541415557C9,
	IL2CPPLinkage__RoGetActivationFactory_mD9B64895F07EE68FD4BE0408F2E3C45C5E813FFF,
	IL2CPPLinkage__WindowsCreateString_m35546AF02FCD01FF468E130AAA66CBF5F1A39C58,
	IL2CPPLinkage__WindowsGetStringRawBuffer_mB9DF308A4B75EC602FAB5FA66BA6C418F1AA3668,
	IL2CPPLinkage__WindowsDeleteString_m0B3377B15784475F3EBE705FBA60665F70BCB52B,
	IL2CPPLinkage__GetHRForLastWin32Error_mDF1B3A8650831F0DD3A99A78648CF0B945A7664A,
	IL2CPPLinkage_LoadLibraryExW_mC685B6B56B732D1A710BA06AC0849F6BBD6283F6,
	IL2CPPLinkage_FreeLibrary_m71CA7FAB535475C4178AF8EA5526F0247F0AFB85,
	IL2CPPLinkage_GetProcAddress_mC672CC99DBA693AAA988C599885974F3153E07F3,
	IL2CPPLinkage_CoIncrementMTAUsage_m3AC500BB7A8303FC8A01855FA1CFD78BC15EBD08,
	IL2CPPLinkage_CoDecrementMTAUsage_mD2E7E2194EFA0D41C486435911ED39FA10E80F3D,
	IL2CPPLinkage_RoGetActivationFactory_m910D66A79FFD913A25B2A880CD89BEA3A70FD028,
	IL2CPPLinkage_WindowsCreateString_m4233064DE1FAD27B3EA41149F96857B1B0D8589B,
	IL2CPPLinkage_WindowsGetStringRawBuffer_mB65B56D4E11B9A88D352DD0E1160B4FC158FC1E4,
	IL2CPPLinkage_WindowsDeleteString_mF7B2093CA919DBAEB8AE95886A9D21E11D00766A,
	IL2CPPLinkage__ctor_mAAA91BAD42E2A20E2921BE2A1CF558AA45F4C489,
	Mono_mono_thread_current_mB8EA4881B220B2B07CBCE6AD3F3B4E035A2B3B37,
	Mono_mono_thread_is_foreign_m532A9C01D3C8DE57846641416A5EB6CC5CBE3707,
	Mono__cctor_mD78AA90AD75DBB2269B75A1DCB31283735350578,
	mono_thread_cleanup_initialize__ctor_mD97F789C38AB8E6D4C31E902622E29CECF71C23B,
	mono_thread_cleanup_initialize_Invoke_m506DF1E7B280E525DAE60B58BB941B3AC03350C3,
	mono_thread_cleanup_initialize_BeginInvoke_m42FAB2BC583B72B7F8ED4F76CC0E5C39DE8FCE7C,
	mono_thread_cleanup_initialize_EndInvoke_m153757EE9525E0E15292481E4EE42B3F793B6E1E,
	mono_thread_cleanup_register__ctor_m2D9E73E4D69D312AABAD5D6A3C7E76C3CAA6F5E4,
	mono_thread_cleanup_register_Invoke_m3C2E54E14802CA53B60FF8EFEB4062D9F42B7F3D,
	mono_thread_cleanup_register_BeginInvoke_mF701B4C4D09AA7ECC3C9D24F9E1C3C8D7855861F,
	mono_thread_cleanup_register_EndInvoke_m8559864DE7F481041F92ED12D3147DDF442E260D,
	ThreadContext__cctor_mF106BE5E551303EE7D66B2171D8E6CC83DB6F23F,
	ThreadContext__ctor_m82841A2030C09E0334445CC697481418FD0BAB83,
	ThreadContext_Dispose_m8A8F13BA3512314E45EFA48FA4E14D5A813D6B25,
	ThreadContext_RegisterPossiblyNonForeignThread_mEE6D432A9E28F666229782C4E6860A6808C5F095,
	U3CU3Ec__cctor_m66FE8C536731096F510810716A267DC6C062982C,
	U3CU3Ec__ctor_m6B00E9E2B08B783B313649A2D4D6002AB33294DA,
	U3CU3Ec_U3C_cctorU3Eb__7_0_m37576E4BE02F6CA6E17343992B3C084D76F8A1CE,
	HString__ctor_m54AF7CF8C00F028D556D223C6407BCF9C54216A1,
	HString__ctor_m58FE025116D93D2212CA985FD43D37FCD7FCCAF4,
	HString_ToString_m3067C77765233CFA5DABACD0A81974B9353F7DE9,
	HString_Dispose_mE78C86E9299020D162C80AE00E8D8E12E21AFED6,
	ModuleReference_AllocateRefCount_m9504F540CF8B75AD5437A4FF92839BED7FD68667,
	ModuleReference_Allocate_m7C319500124AE2925324756B62DABA2F0F32EBCB,
	ModuleReference_get_Null_m1DB8ADCC56ECF811AFDFA4B8A285A76D58F6E021,
	ModuleReference__ctor_mD5D1FE57CCD13CDF667A138B5B1D1052474F18F6,
	ModuleReference_AddRef_m5136A0E46D35FF37C8A21CE63E3ED579143ADDD5,
	ModuleReference_Release_mEC3B2EBE3AC8A98FF9EE3B1F7F22F07D0252C48F,
	IObjectReference_get_VftblIUnknown_m5279C0FFD5E238BEA15698C21165E061D791D588,
	IObjectReference__ctor_m345B807A7F725DA0C786C31FB6FCC74DDA1B69B8,
	NULL,
	NULL,
	IObjectReference_Finalize_mFA07314332B5CB3E058326802B56400AEE803F74,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DllModule__cctor_mAA19375698230E3B8B60DAD54F53287D0746E3F5,
	DllModule_TryLoad_mD630F812F1BB2488BC47EB3C5489AD09E7375DC3,
	DllModule__ctor_m39518967C67DEDC1B3957B418077792DDEB3A252,
	DllModule_GetActivationFactory_mAE91D0B4A53B6D65EB24F967C61559BA2259E615,
	DllModule_Finalize_mD6E7D99BDD6EBD66342B3B3E1C732263D1F44FDB,
	DllGetActivationFactory__ctor_m8AB17E1A8A01BC5D47F9FFC15E320A37B8E6B866,
	DllGetActivationFactory_Invoke_m295F421E5BC950F118C574B619F96F5E3B9E7529,
	DllGetActivationFactory_BeginInvoke_m7F8C6F01BBF80C8D4B54874373288EFE308DE4BF,
	DllGetActivationFactory_EndInvoke_m24A18C458A1DC34EC41D7174293E6862EEF3D4E4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WinrtModule_get_Instance_m2065DD5CF15F1673BAC8A50329CF20E3590455DC,
	WinrtModule__ctor_mCCB8A9F71AEEC91E94D6CB71F2FDA285759CF751,
	WinrtModule_GetActivationFactory_m1EFCA4645510CE8AC772D33664C8271B43BFA110,
	WinrtModule_Finalize_mDDC9B93E0CEBB76E7D0D5363D7CC7AED57B45D56,
	WinrtModule__cctor_m4C4F8589C5F94E8DDA5DEB2C38F8E0371BF89610,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Delegate_FindObject_mFA0830E73AEB1315518CF293E7F98281850DD455,
	Delegate_QueryInterface_m28CBEE6454B0F6D54F4190D317C063B5E8DBC054,
	Delegate_AddRef_mF193494E0821B8267341BD17F8D1F55C6323C649,
	Delegate_Release_m15D852676BDBCA307BF3420F6E23FC44A5E6B471,
	Delegate_QueryInterface_m6467DA44D2AAC6A55349DC013423E3F9618B3D12,
	Delegate_AddRef_m1565DF1BCF72D5ABA3E3FA9BA795D02D576A186D,
	Delegate_Release_mDFACF297B26CCB4EE0FA99927B81568B13099712,
	NULL,
	Delegate__cctor_mA76D7C7A3730B353DC79522007A81D158289706D,
	Delegate__ctor_mC3882A4DA0657F017EC039901A0D03BFC2B3ED7E,
	Delegate_Finalize_m3859A6A93825D86B75B3569E2E8D6FE599C636DC,
	Delegate__Dispose_mDBB0DF139255C2DBB123AF2C4CF42FE060423EB3,
	InitialReference_get_DelegatePtr_mCBEFA2C039F64556D7C2B80ACBF57EA998B92302,
	InitialReference__ctor_m7D478E29D9482CBF95C7D1430F13261E36B527F2,
	InitialReference_Finalize_m544A4D8DDB1BC2F826D57AA8E5320F2C46C2F507,
	InitialReference_Dispose_m0C431CDD0BDA7B71144D9E4CEA0EF44D246C6778,
	EventSource__invoke_m122079F494464119D24BCDDCB5F785018CA461C5,
	EventSource__ctor_mF1DAA42E58CEC58571516A8E2CEAA5034E4759E2,
	EventSource__cctor_m200EE6B9CBAC6F6A5C1B4146AE7C1C464198C248,
	_Invoke__ctor_m96320F48526CDD119B7B64EEDE72CD8CCD9E896E,
	_Invoke_Invoke_m67B75423B6F02F21276698C37708D10FB53A38AC,
	_Invoke_BeginInvoke_mF36C5FE792DB02342672AB205181C33C3FFE2206,
	_Invoke_EndInvoke_m2D42659A23F517777933C61E7D95947857447F30,
	U3CU3Ec__DisplayClass1_0__ctor_m22350EA1ABD075E9B82A524FE2E6A2A030A2B569,
	U3CU3Ec__DisplayClass1_0_U3C_invokeU3Eb__0_m35FD60471B7BC183C27E1188E84018000C5A6F42,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoPInvokeCallbackAttribute__ctor_m915A08C30E913EFB4BC61E6E9F483A61A336BD9F,
	MonoPInvokeCallbackAttribute_set_DelegateType_mE2444C6D7B8CB7CEB07566E7FB01784F9712F9FA,
	_QueryInterface__ctor_mD68BF814E2217323EC027913028D8CB8B36088C8,
	_QueryInterface_Invoke_m7F69C0E3D2A4B387CB35852201325A59138A6F9C,
	_QueryInterface_BeginInvoke_mA65FC1BEF9A8BDF9B09D72068CC58360081AB038,
	_QueryInterface_EndInvoke_m385BF355FE1242A895E6875856F815461B5C202A,
	_AddRef__ctor_m60659BD2453C94835DA466611328E42B16425A2D,
	_AddRef_Invoke_m2AF40C1C641ED0FA34BBD81567ABA691DA994C70,
	_AddRef_BeginInvoke_m791C36E95C29666B4C024B367F2367CAEE05D403,
	_AddRef_EndInvoke_mAA71F0A1BAACA74349B5F557F7B090444DD37BF7,
	_Release__ctor_mE49ED0E9447B8425E065438576CBEBEF124A6FB4,
	_Release_Invoke_m01B8A4F72B103E72CBC07062F5B8C779F6BAF9F5,
	_Release_BeginInvoke_mCCEF6D5A6426A68C514CECCEAEC07F2430FDDBBF,
	_Release_EndInvoke_mB157310C130D824021B7923C15A515CF95D80287,
	_GetIids__ctor_m3927D8323DF10F42D92BB19B6F3913B3EB3F377A,
	_GetIids_Invoke_mB460E8E2BC2DDE592B667E88D50B6188619479A8,
	_GetIids_BeginInvoke_m0223A3CC43D725E866E965030F89EB0E06962479,
	_GetIids_EndInvoke_mA1619393B649307371C51D039138DFE938E81264,
	_GetRuntimeClassName__ctor_m7DBA37FEDE9D059EF0CC6A7E464F4652DDB585BF,
	_GetRuntimeClassName_Invoke_m4D73D5F05B166C9E76C9806EF9E59D1B472BD188,
	_GetRuntimeClassName_BeginInvoke_mE8B4EB3D32D2944F22E03F06700C65060616C1CB,
	_GetRuntimeClassName_EndInvoke_mEBC65041A6B5B2EDC531D9EC0F0EACF9117FCD85,
	_GetTrustLevel__ctor_mEBC3CD6FF5069E35916B2603809587F20DBAC27F,
	_GetTrustLevel_Invoke_m61C4F3C6B1E37253AA6CF120FF72A82A5013DA4B,
	_GetTrustLevel_BeginInvoke_mB5E1DB180FC10F5B94FDD96C3DDEE15BE880BFCC,
	_GetTrustLevel_EndInvoke_m730CBCA9351A942865586EDEE5371A646F68580F,
	_ActivateInstance__ctor_mF8806B7C77EF673468D808E5BC4D53BC19F4A77A,
	_ActivateInstance_Invoke_mB08503B0ADF6DD6FEA50632E78A60FF4BF35D6A4,
	_ActivateInstance_BeginInvoke_m27D683CB18AC085880F879D26EE91CCF210A0931,
	_ActivateInstance_EndInvoke_m70C5C9470E079476EFC29CFDA01AC189B9E3AF00,
	_Bool_op_Implicit_m76F768EE291283874AA5CF28D78DE6949EF1A116,
	_get_PropertyAsInt__ctor_mDD70D3194BACC86D5EF741EB608BBBAAD9711EAF,
	_get_PropertyAsInt_Invoke_mA2CB9549CA91604196249944CC34B6F518C15E5F,
	_get_PropertyAsInt_BeginInvoke_m5567B2037276D6EF16DE078443BBF5D71FE93633,
	_get_PropertyAsInt_EndInvoke_m7FD377AAB498DC759FD0A62C4DB52D85184F9188,
	_get_PropertyAsUInt__ctor_mA6B3292830A2E51BBDCDD0BBBB0357BA2AF15835,
	_get_PropertyAsUInt_Invoke_mEF9E93E0B6F5D5280579B4723C7BC2BA514E4800,
	_get_PropertyAsUInt_BeginInvoke_mB17791AE904E0072103D2D17470D7290C59C1172,
	_get_PropertyAsUInt_EndInvoke_mE42F919AF4C9713D29237702339BEA959BB09E8D,
	_get_PropertyAsBool__ctor_m3EE433788D58015F949EC3C8696453CE73EDAE97,
	_get_PropertyAsBool_Invoke_m3C98EEE4D236E038ADB9D16A9D7438FE533C38FA,
	_get_PropertyAsBool_BeginInvoke_m0114D04318E0BB40B8274FD6FD6FFE645A937DEB,
	_get_PropertyAsBool_EndInvoke_m517BF03B243B7B394A0815434B41DB7DE9468F47,
	_get_PropertyAsFloat__ctor_m5478F78469497B7BDA57B46538028868C43FFBF5,
	_get_PropertyAsFloat_Invoke_m2C75BDB217D8FCDE434CAE0E194CEECA594891CF,
	_get_PropertyAsFloat_BeginInvoke_m46C046675813C3FA76CB8B3E9E2386DBB7BF1E8B,
	_get_PropertyAsFloat_EndInvoke_m4DDD95F6CFBA5C2D8010DBF62D8E7E803D0893CB,
	_get_PropertyAsObject__ctor_mD9A46F1655DCBF7AB86C44451B0C6D4BC06677F2,
	_get_PropertyAsObject_Invoke_m1917D8F5BA5A86416C04A0E0FAD38CE827503EA9,
	_get_PropertyAsObject_BeginInvoke_m4891144DE614B249C650221139044B2C193F54AA,
	_get_PropertyAsObject_EndInvoke_mE155EE6CE7E2B3F0EF7ED5218142A99B3D523BCD,
	_put_PropertyAsObject__ctor_mB695938A9D887B64FA231705933D8DE7173B0177,
	_put_PropertyAsObject_Invoke_mDF1D89E8D5364CCF513E0044AFBD071ED450DD0B,
	_put_PropertyAsObject_BeginInvoke_mC041F0BDE7B798F61293772E2597EE1BD278A1D9,
	_put_PropertyAsObject_EndInvoke_mCB62434060807BDDC648A4694556AFF62D738776,
	_get_PropertyAsEnum__ctor_mA694C57389947CD3E2511FDB9525054C5A38E9FE,
	_get_PropertyAsEnum_Invoke_mA3E0555F3A72CAF26FE3CB281262E1333073A3AA,
	_get_PropertyAsEnum_BeginInvoke_m578435201CB3224D9AFF9C4C98B6575F9646B44D,
	_get_PropertyAsEnum_EndInvoke_mA3AFF6E24299B0D14C5B119CA94E04E3CE3521EA,
	_get_PropertyAsGuid__ctor_m73E55A34DCB8AB768A8C1C537C8F21AE47F91C89,
	_get_PropertyAsGuid_Invoke_mC7D08CDB9AE9B1464D8BB683A07D1E85B36E987C,
	_get_PropertyAsGuid_BeginInvoke_m617C12B5DEB0A5C3696C8D9DA6A3D51E98180F23,
	_get_PropertyAsGuid_EndInvoke_m1DD54E4F9AFFEE6DAE85E218C3F9F2580DC2C362,
	_get_PropertyAsDateTime__ctor_m725E9CDDC0C23D119E125DC4D0909DA4FE234727,
	_get_PropertyAsDateTime_Invoke_mC79711A23B5E8DC84E46390DF883E05126624A31,
	_get_PropertyAsDateTime_BeginInvoke_mFF637BDDB82458912F1F6AF8EC6037CA7E51C8AD,
	_get_PropertyAsDateTime_EndInvoke_m386A4E9BC2AEC626CB2685F8AAEDF53EBCD74355,
	_get_PropertyAsTimeSpan__ctor_m5B4FC792F5263385DF8EB01379107208FDA62F84,
	_get_PropertyAsTimeSpan_Invoke_m55E6AFDF1EF562CF51E0690D80CCEFFBAB135D96,
	_get_PropertyAsTimeSpan_BeginInvoke_m8E875D2353F52EC9F9B94F281E28E0984A43FCFF,
	_get_PropertyAsTimeSpan_EndInvoke_m3D258C5DC0E2BCFE0AE8B658E3286E23E57F9712,
	_add_EventHandler__ctor_mCAA996DED22A25CFE3245E3D86319EDA93B9556A,
	_add_EventHandler_Invoke_mD4BDAA8CEF55F4ECC1E6F8D5110EFCC50561B48D,
	_add_EventHandler_BeginInvoke_mEE2A5DDFBE9F30FCD1156AEC48B32D81BE4553E4,
	_add_EventHandler_EndInvoke_m958E5F83A23CDAEA8F23F3E70AA4E005B50CE633,
	_remove_EventHandler__ctor_m410B32951AC0477566CCFE04FB07D4E223A690E6,
	_remove_EventHandler_Invoke_m3D8F86DED311BDBB555A31FCEFE25EB9AE53ACEC,
	_remove_EventHandler_BeginInvoke_mDCC3991FEED00E07E9AE187DD3AED334737055EA,
	_remove_EventHandler_EndInvoke_m880B2E61D4FF2EB2338E6F41AEDF28B116CA1B4B,
	_method0__ctor_mF45D4765F0CC1C4AB0046CE7B9047B9524AA8A58,
	_method0_Invoke_m5BA392D9F9112B38345B2A6FEA907A62EA2ED602,
	_method0_BeginInvoke_m0D76CD50246644B8D98FD95E1C04E3A7B77221F7,
	_method0_EndInvoke_m8FA7FD1243B8E7F1060422BF60D22ECC13EBC5B6,
	IDelegate2_Obj_Obj__ctor_m0D3CF7401ED981769154DC7772D9C4FC163C8501,
	IDelegate2_Obj_Obj_Invoke_m585BD2DE510F03C1EC0C7217A67C5B2EDB864D8A,
	IDelegate2_Obj_Obj_BeginInvoke_m4A18C2480FDA5F71E07F31026E5AD42254B193A2,
	IDelegate2_Obj_Obj_EndInvoke_mAC234DFF0459C5A7A3BCE892FEBBA0CDBD105D96,
	IDelegate2_Obj_Enum__ctor_m13EDE19A4F89CA35FA9B17EFF088ED5B043F96B1,
	IDelegate2_Obj_Enum_Invoke_m911F5B11055AB8307E957BC41974A893D2F63A9E,
	IDelegate2_Obj_Enum_BeginInvoke_m16FDE209FE6E1EC9DCD8C16464D6E13F34308A7F,
	IDelegate2_Obj_Enum_EndInvoke_m1884125D339924E560796FCBF0E4437A325CDFA6,
	NULL,
	NULL,
	IAsyncInfo_op_Implicit_m16C479275A56E345498B0DED78B3D94675617444,
	IAsyncInfo_op_Implicit_mDF153DE0BB13D327BD541D5BD8F0A03484C7444E,
	IAsyncInfo__ctor_m5F3E8229900F0999503495D7B0C5CB37C42CE143,
	IAsyncInfo_get_ErrorCode_m1C25DABFC9FDBBC4446E9583206E7DBAE3DD500A,
	IAsyncInfo_Cancel_m09826943CD94AA1DBF8DD7E1D83B41415F2A7A5F,
	IAsyncOperation__ctor_m257FF1710789A572935C52FCAC888CC02C496029,
	IAsyncOperation__OnCompleted_m20528AE352B798EDDBFD33AA9064E6BFF7EB60C3,
	IAsyncOperation_set_Completed_m662E47E70A763D519B9948DDEFB2F478E5EF216D,
	IAsyncOperation__cctor_m6CAD2C06113F8204912825C8686EFA489DABFB92,
	CompletedHandler__ctor_mADCC48FAE51187931A2EB33CBA580F8C61E33428,
	CompletedHandler_Invoke_m3390A5EC8FBAEDB455755F72811899725EFFF0E6,
	CompletedHandler_BeginInvoke_m87C75727ED4E2F6A162ED4294483FC833493F3E6,
	CompletedHandler_EndInvoke_m3D066A294908D3068444F85F293DF7010CAEA359,
	U3CU3Ec__DisplayClass4_0__ctor_mE7DE39605653458D324B60FB3E71310EBFDA3398,
	U3CU3Ec__DisplayClass4_0_U3C_OnCompletedU3Eb__0_m4BEE91D6307A33E33FB893A4AC781EE855E42FDF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IQRCode_op_Implicit_mC303682F5AEBAB4467BCE37E49F493A5810D4CD6,
	IQRCode_op_Implicit_mD02E163196243DC5350493515129A1A7CA8610F7,
	IQRCode__ctor_mE6045DB0E12EEBFB093D6C844BBB44078DA893A9,
	IQRCode_get_Id_m2D3C9AA61F411F3343E815BCF3303D47DE66A911,
	IQRCode_get_SpatialGraphNodeId_m76D97656F7E3666EB37178C846F5201F975CF2A3,
	IQRCode_get_Version_mE034619125C1C0426C5095C97605C810042476F1,
	IQRCode_get_PhysicalSideLength_mEF5BB92B87413DBB71E6AB10588C454504639A19,
	IQRCode_get_Data_m3E2598ECE88F0CD99EA497902A20702DC8501ED0,
	IQRCode_get_SystemRelativeLastDetectedTime_m218B0E8028C232BFA482C610D0FF00076CEF185D,
	IQRCode_get_LastDetectedTime_m0B0D02A7BCB4D86FFF2AD848B1BF3CBAA215FF91,
	_GetRawData__ctor_mC95057AB611E0A11BB91681331B3532D550DF778,
	_GetRawData_Invoke_m0028AC4CB8B33E72445A870B584257A4A7360053,
	_GetRawData_BeginInvoke_m2A2250655B4A6A9621F06DD4EE6D56C86BE4472F,
	_GetRawData_EndInvoke_mEB2419F85E526F1BF7E44408A0EAB27FCB61AE24,
	QRCode__ctor_mDA586215D17042FEA4F256EEACD5D8B5F31AA788,
	QRCode_get_Id_mE3FB5E809784AD1A16D3C9792089E36BF3EDA4EB,
	QRCode_get_SpatialGraphNodeId_m0C034B923BE48C3396269023364DEF35A94D388C,
	QRCode_get_Version_m720D2408C3EDBA82EC2C281ECCE2E48B9562C00C,
	QRCode_get_PhysicalSideLength_mC7959DEEBE81AFB20E5BFD03851C7E09035B7E41,
	QRCode_get_Data_m1FC66FDD0F49E9F9D918648349318AC1E1986C50,
	QRCode_get_SystemRelativeLastDetectedTime_m0A1D9CBC227CEFC4D9622908C0F6A068EEB3EF8C,
	QRCode_get_LastDetectedTime_mFDA34B2DA60810867A81753D8E11C18AFC7B891A,
	IQRCodeAddedEventArgs_op_Implicit_mF7FFC524B78EB499F15A973B87B5562A4F940B54,
	IQRCodeAddedEventArgs__ctor_mFCBD2EB66AE675E9AF66CC45DCE4198E7CFD7D35,
	IQRCodeAddedEventArgs_get_Code_m261A030AF42A1C737992DD410C75DF0CBBF24880,
	QRCodeAddedEventArgs__ctor_m23959E9410B31231059AF5F84A1496454402FE2C,
	QRCodeAddedEventArgs_get_Code_mE9EABF426E43F7F28BD0A029DE2CABD2EB022A98,
	IQRCodeUpdatedEventArgs_op_Implicit_mDC1DA085E3144034987F9B9F395316F1611BAB04,
	IQRCodeUpdatedEventArgs__ctor_mA50C0647EFDACEABEE10CB72C83AE91521B64878,
	IQRCodeUpdatedEventArgs_get_Code_m9E0592C6FFC43D307FA4A8F46BC846FA846E1D97,
	QRCodeUpdatedEventArgs__ctor_m2ACB70F5CB45FBC028B3794D603F3A1F99547781,
	QRCodeUpdatedEventArgs_get_Code_m768700916CA1AC3F786B4B6AD6FE2F7D2A4CFA6E,
	IQRCodeRemovedEventArgs_op_Implicit_m09ACBC4DE6D7E030D1BDF7A46335795A07264830,
	IQRCodeRemovedEventArgs__ctor_mF00B77CF8CC0DFF8861F180A3F65625528B2D744,
	IQRCodeRemovedEventArgs_get_Code_mF5D3C8EAC6FF98FE702A13410DC19EF6821D0878,
	QRCodeRemovedEventArgs__ctor_m90DFD3C5166DACB7881CFC454F5A4AC02A725ECC,
	QRCodeRemovedEventArgs_get_Code_m7C505245A79F9A072B6134CF0971E4D444B08E94,
	IQRCodeWatcher_op_Implicit_m9B117FF748971AE401014678C515CC439BDD106C,
	IQRCodeWatcher__ctor_m37BC9B21B5AD1867C2A8440EF08903284829281F,
	IQRCodeWatcher_Start_m4C5AC51B29DC53D2A4A1546E63896179ED66A396,
	IQRCodeWatcher_Stop_m4E45FEABF05F18CE4C06A6E852B52EA99F9B67FF,
	U3CU3Ec__cctor_m9725FEA2D1F1EED2E01193C1B06D8CE17EC5C477,
	U3CU3Ec__ctor_m130720570E2D4FAAF66D5FD1925C5F055ACDF9DD,
	U3CU3Ec_U3C_ctorU3Eb__5_0_m2853C1F50981B6B7B0A012041EA9751D91CDD62B,
	U3CU3Ec_U3C_ctorU3Eb__5_1_mFA8C3690728543E8D2FED51F276E3B957021614C,
	U3CU3Ec_U3C_ctorU3Eb__5_2_m36DB53B9D6A42ABB19076134730CBE911A20CAED,
	U3CU3Ec_U3C_ctorU3Eb__5_3_m197CDFD32C58CBFA33FC80E260E0FD2E2DC11FB7,
	IQRCodeWatcherStatics__ctor_mE76E870AC41F6FA207116C82F1EF1BB44B752943,
	IQRCodeWatcherStatics_IsSupported_m942EB3628449DA3DB35D7193437B3FFD71BE4670,
	IQRCodeWatcherStatics_RequestAccessAsync_m490DAD715199242DA175A65E13B38BEDE0B0EA5A,
	_RequestAccessAsync__ctor_m533C80BD0BB82AC1C49EC87479811F4FF0919B1A,
	_RequestAccessAsync_Invoke_mCB5B76596DAD516FBAEDF9CCE615071E0C5D7B2B,
	_RequestAccessAsync_BeginInvoke_m45E12D5B1E8B24A5DF7D8933F0105E5EDE121A5E,
	_RequestAccessAsync_EndInvoke_m4A4A44C8046EABFBB7B5D716C56AD18016A1A150,
	U3CU3Ec__cctor_m3493D132B190D872A69D0225E1B618C0483D6CDF,
	U3CU3Ec__ctor_mA0A352B377891454345F2AF737437E14166B0FA1,
	U3CU3Ec_U3CRequestAccessAsyncU3Eb__6_0_m7EDD33D4947620D2B4AA9AA549C03993EB52A3CD,
	QRCodeWatcher_IsSupported_m9B155614ABBE5A10B5C79AB65A25678B7DC69E5A,
	QRCodeWatcher_RequestAccessAsync_m9F39960B5369B59D81F9ECC8D11581A775CB2BDF,
	QRCodeWatcher__ctor_mDBD69C29C79CAA338D54ACDB4FBA4B0F019A2BBD,
	QRCodeWatcher__ctor_mB6A3F7902F8F8C24C9B2A380369D7D5113C4FF09,
	QRCodeWatcher_Start_m032F0A1254BFA694CA0D9C2076B41427279FFDEF,
	QRCodeWatcher_Stop_m1AF8DE0E90588672674A4826712BED22057E9842,
	QRCodeWatcher_add_Added_m8EA1CC941860DB9FC7374F2FA915E2DA6EE07A40,
	QRCodeWatcher_remove_Added_m3150FFE5E8A235082A3D496ECE6F67391AC8A9EB,
	QRCodeWatcher_add_Updated_mB77CE150EFB8DAC8BE2577024445FD957D709A25,
	QRCodeWatcher_remove_Updated_m3FC6201AB7FF343BE1502FFB7B4F4724A5BCB221,
	QRCodeWatcher_add_Removed_m7F660B5AF724FBF88A409F25578F95EB29100556,
	QRCodeWatcher_remove_Removed_m16384F8FCCE8A9C04B84B659DE97C51AD20896E5,
	QRCodeWatcher_add_EnumerationCompleted_mD126E6A8F90E20D89D05BB6834510EF8C0FCC89C,
	QRCodeWatcher_remove_EnumerationCompleted_m3DCB61192F38A71BA1542689A69AD35BEF8ECEE5,
	QRCodeWatcher__cctor_m6B0988DF823FB258F2F98B96C3DC7F12D4F663A8,
	Statics__ctor_m9C94C85B4E194448E31E44BE08D41F9EBB64E21C,
};
extern void ModuleReference__ctor_mD5D1FE57CCD13CDF667A138B5B1D1052474F18F6_AdjustorThunk (void);
extern void ModuleReference_AddRef_m5136A0E46D35FF37C8A21CE63E3ED579143ADDD5_AdjustorThunk (void);
extern void ModuleReference_Release_mEC3B2EBE3AC8A98FF9EE3B1F7F22F07D0252C48F_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x0600005A, ModuleReference__ctor_mD5D1FE57CCD13CDF667A138B5B1D1052474F18F6_AdjustorThunk },
	{ 0x0600005B, ModuleReference_AddRef_m5136A0E46D35FF37C8A21CE63E3ED579143ADDD5_AdjustorThunk },
	{ 0x0600005C, ModuleReference_Release_mEC3B2EBE3AC8A98FF9EE3B1F7F22F07D0252C48F_AdjustorThunk },
};
static const int32_t s_InvokerIndices[363] = 
{
	23,
	23,
	3,
	2295,
	25,
	-1,
	-1,
	732,
	25,
	2296,
	2297,
	2298,
	25,
	138,
	2299,
	1143,
	2300,
	1150,
	2301,
	2302,
	2303,
	2304,
	2301,
	10,
	2299,
	1143,
	2300,
	1150,
	2301,
	2302,
	2303,
	2304,
	2301,
	10,
	2295,
	555,
	874,
	457,
	130,
	2305,
	2306,
	2298,
	130,
	23,
	2299,
	1143,
	2300,
	1150,
	2301,
	2302,
	2303,
	2304,
	2301,
	10,
	2295,
	555,
	874,
	457,
	130,
	2305,
	2306,
	2298,
	130,
	23,
	732,
	555,
	3,
	111,
	533,
	2307,
	26,
	111,
	7,
	802,
	26,
	3,
	23,
	23,
	3,
	3,
	23,
	95,
	26,
	6,
	14,
	23,
	2308,
	2309,
	2310,
	2311,
	2312,
	23,
	2313,
	2314,
	-1,
	-1,
	23,
	95,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	0,
	2315,
	28,
	23,
	111,
	2316,
	2317,
	123,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2310,
	23,
	0,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	18,
	2305,
	130,
	130,
	2318,
	10,
	10,
	-1,
	3,
	2319,
	23,
	23,
	15,
	2319,
	23,
	23,
	2320,
	23,
	3,
	111,
	2321,
	2322,
	26,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	111,
	2302,
	2323,
	123,
	111,
	2301,
	802,
	123,
	111,
	2301,
	802,
	123,
	111,
	2302,
	2323,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	2324,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2325,
	2322,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2316,
	2317,
	123,
	111,
	2326,
	2327,
	123,
	111,
	2328,
	2329,
	123,
	111,
	2301,
	802,
	123,
	111,
	2330,
	2331,
	123,
	111,
	2332,
	2333,
	123,
	-1,
	-1,
	0,
	0,
	26,
	14,
	23,
	1431,
	2334,
	26,
	3,
	111,
	32,
	554,
	26,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	0,
	0,
	26,
	442,
	442,
	10,
	692,
	14,
	284,
	1824,
	111,
	2335,
	2336,
	123,
	26,
	442,
	442,
	10,
	692,
	14,
	284,
	1824,
	0,
	26,
	14,
	26,
	14,
	0,
	26,
	14,
	26,
	14,
	0,
	26,
	14,
	26,
	14,
	0,
	26,
	23,
	23,
	3,
	23,
	953,
	953,
	953,
	953,
	26,
	95,
	14,
	111,
	2316,
	2317,
	123,
	3,
	23,
	37,
	49,
	4,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	3,
	23,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[5] = 
{
	{ 0x06000087, 2,  (void**)&Delegate_QueryInterface_m28CBEE6454B0F6D54F4190D317C063B5E8DBC054_RuntimeMethod_var, 0 },
	{ 0x06000088, 3,  (void**)&Delegate_AddRef_mF193494E0821B8267341BD17F8D1F55C6323C649_RuntimeMethod_var, 0 },
	{ 0x06000089, 4,  (void**)&Delegate_Release_m15D852676BDBCA307BF3420F6E23FC44A5E6B471_RuntimeMethod_var, 0 },
	{ 0x06000096, 5,  (void**)&EventSource__invoke_m122079F494464119D24BCDDCB5F785018CA461C5_RuntimeMethod_var, 0 },
	{ 0x0600010E, 6,  (void**)&IAsyncOperation__OnCompleted_m20528AE352B798EDDBFD33AA9064E6BFF7EB60C3_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[20] = 
{
	{ 0x02000013, { 8, 3 } },
	{ 0x02000016, { 11, 10 } },
	{ 0x02000017, { 21, 4 } },
	{ 0x02000019, { 25, 7 } },
	{ 0x0200001A, { 36, 3 } },
	{ 0x02000021, { 41, 10 } },
	{ 0x02000042, { 51, 1 } },
	{ 0x0200004A, { 52, 9 } },
	{ 0x0200004B, { 61, 3 } },
	{ 0x0200004C, { 64, 5 } },
	{ 0x0200004D, { 69, 4 } },
	{ 0x06000006, { 0, 2 } },
	{ 0x06000007, { 2, 2 } },
	{ 0x0600005F, { 4, 2 } },
	{ 0x06000060, { 6, 2 } },
	{ 0x0600007E, { 32, 1 } },
	{ 0x0600007F, { 33, 1 } },
	{ 0x06000080, { 34, 1 } },
	{ 0x06000081, { 35, 1 } },
	{ 0x0600008D, { 39, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[73] = 
{
	{ (Il2CppRGCTXDataType)1, 45158 },
	{ (Il2CppRGCTXDataType)3, 49634 },
	{ (Il2CppRGCTXDataType)1, 45159 },
	{ (Il2CppRGCTXDataType)3, 49635 },
	{ (Il2CppRGCTXDataType)1, 45189 },
	{ (Il2CppRGCTXDataType)3, 49636 },
	{ (Il2CppRGCTXDataType)3, 49637 },
	{ (Il2CppRGCTXDataType)2, 45190 },
	{ (Il2CppRGCTXDataType)2, 45196 },
	{ (Il2CppRGCTXDataType)3, 49638 },
	{ (Il2CppRGCTXDataType)3, 49639 },
	{ (Il2CppRGCTXDataType)2, 49132 },
	{ (Il2CppRGCTXDataType)3, 49640 },
	{ (Il2CppRGCTXDataType)2, 45206 },
	{ (Il2CppRGCTXDataType)3, 49641 },
	{ (Il2CppRGCTXDataType)3, 49642 },
	{ (Il2CppRGCTXDataType)2, 49133 },
	{ (Il2CppRGCTXDataType)3, 49643 },
	{ (Il2CppRGCTXDataType)3, 49644 },
	{ (Il2CppRGCTXDataType)3, 49645 },
	{ (Il2CppRGCTXDataType)3, 49646 },
	{ (Il2CppRGCTXDataType)2, 49134 },
	{ (Il2CppRGCTXDataType)3, 49647 },
	{ (Il2CppRGCTXDataType)2, 49134 },
	{ (Il2CppRGCTXDataType)3, 49648 },
	{ (Il2CppRGCTXDataType)1, 45230 },
	{ (Il2CppRGCTXDataType)2, 45229 },
	{ (Il2CppRGCTXDataType)3, 49649 },
	{ (Il2CppRGCTXDataType)2, 49135 },
	{ (Il2CppRGCTXDataType)3, 49650 },
	{ (Il2CppRGCTXDataType)2, 49136 },
	{ (Il2CppRGCTXDataType)3, 49651 },
	{ (Il2CppRGCTXDataType)3, 49652 },
	{ (Il2CppRGCTXDataType)3, 49653 },
	{ (Il2CppRGCTXDataType)3, 49654 },
	{ (Il2CppRGCTXDataType)3, 49655 },
	{ (Il2CppRGCTXDataType)2, 49137 },
	{ (Il2CppRGCTXDataType)3, 49656 },
	{ (Il2CppRGCTXDataType)2, 49137 },
	{ (Il2CppRGCTXDataType)2, 45238 },
	{ (Il2CppRGCTXDataType)3, 49657 },
	{ (Il2CppRGCTXDataType)2, 45258 },
	{ (Il2CppRGCTXDataType)3, 49658 },
	{ (Il2CppRGCTXDataType)1, 45263 },
	{ (Il2CppRGCTXDataType)3, 49659 },
	{ (Il2CppRGCTXDataType)3, 49660 },
	{ (Il2CppRGCTXDataType)3, 49661 },
	{ (Il2CppRGCTXDataType)3, 49662 },
	{ (Il2CppRGCTXDataType)2, 45264 },
	{ (Il2CppRGCTXDataType)3, 49663 },
	{ (Il2CppRGCTXDataType)3, 49664 },
	{ (Il2CppRGCTXDataType)2, 45344 },
	{ (Il2CppRGCTXDataType)3, 49665 },
	{ (Il2CppRGCTXDataType)2, 49138 },
	{ (Il2CppRGCTXDataType)3, 49666 },
	{ (Il2CppRGCTXDataType)2, 45371 },
	{ (Il2CppRGCTXDataType)3, 49667 },
	{ (Il2CppRGCTXDataType)3, 49668 },
	{ (Il2CppRGCTXDataType)3, 49669 },
	{ (Il2CppRGCTXDataType)3, 49670 },
	{ (Il2CppRGCTXDataType)2, 45373 },
	{ (Il2CppRGCTXDataType)3, 49671 },
	{ (Il2CppRGCTXDataType)3, 49672 },
	{ (Il2CppRGCTXDataType)3, 49673 },
	{ (Il2CppRGCTXDataType)1, 49139 },
	{ (Il2CppRGCTXDataType)3, 49674 },
	{ (Il2CppRGCTXDataType)2, 45383 },
	{ (Il2CppRGCTXDataType)3, 49675 },
	{ (Il2CppRGCTXDataType)3, 49676 },
	{ (Il2CppRGCTXDataType)3, 49677 },
	{ (Il2CppRGCTXDataType)2, 45389 },
	{ (Il2CppRGCTXDataType)2, 49140 },
	{ (Il2CppRGCTXDataType)3, 49678 },
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_QR_DotNetCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_QR_DotNetCodeGenModule = 
{
	"Microsoft.MixedReality.QR.DotNet.dll",
	363,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	5,
	s_reversePInvokeIndices,
	20,
	s_rgctxIndices,
	73,
	s_rgctxValues,
	NULL,
};
