﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000009 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Intersect(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::IntersectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000010 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000013 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000014 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000015 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000018 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000019 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001C System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001D System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001E TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x0000001F System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 (void);
// 0x00000020 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000021 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000022 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000023 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000024 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000025 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000026 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000028 System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000029 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000002A System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002B System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x0000002C System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x0000002D System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x0000002E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002F System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000030 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000031 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x00000035 System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x00000036 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x00000038 System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000039 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x0000003A System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x0000003B System.Boolean System.Linq.Enumerable/<TakeIterator>d__25`1::MoveNext()
// 0x0000003C System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::<>m__Finally1()
// 0x0000003D TSource System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000003E System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000003F System.Object System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000040 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000041 System.Collections.IEnumerator System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000042 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x00000043 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x00000044 System.Boolean System.Linq.Enumerable/<UnionIterator>d__71`1::MoveNext()
// 0x00000045 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally1()
// 0x00000046 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally2()
// 0x00000047 TSource System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000048 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x00000049 System.Object System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x0000004A System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000004B System.Collections.IEnumerator System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004C System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::.ctor(System.Int32)
// 0x0000004D System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::System.IDisposable.Dispose()
// 0x0000004E System.Boolean System.Linq.Enumerable/<IntersectIterator>d__74`1::MoveNext()
// 0x0000004F System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::<>m__Finally1()
// 0x00000050 TSource System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000051 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerator.Reset()
// 0x00000052 System.Object System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerator.get_Current()
// 0x00000053 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000054 System.Collections.IEnumerator System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000055 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000056 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000057 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000058 System.Boolean System.Linq.Set`1::Remove(TElement)
// 0x00000059 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x0000005A System.Void System.Linq.Set`1::Resize()
// 0x0000005B System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x0000005C System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000005D System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000005E System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005F System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000060 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000061 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000062 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000063 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x00000064 TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000065 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000066 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000067 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000068 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000069 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000006A System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000006B System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000006C System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000006D System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000006E System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000006F System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000070 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000071 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000072 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000073 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000074 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000075 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000076 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000077 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000078 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000079 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000007A System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000007B System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000007C System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000007D System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000007E System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000007F System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000081 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000082 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000083 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000084 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000085 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000086 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000087 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000088 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000089 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000008A System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000008B System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000008C System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x0000008D System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x0000008E T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x0000008F System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[144] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[144] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	102,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[44] = 
{
	{ 0x02000004, { 76, 4 } },
	{ 0x02000005, { 80, 9 } },
	{ 0x02000006, { 89, 7 } },
	{ 0x02000007, { 96, 10 } },
	{ 0x02000008, { 106, 1 } },
	{ 0x02000009, { 107, 8 } },
	{ 0x0200000A, { 115, 12 } },
	{ 0x0200000B, { 127, 12 } },
	{ 0x0200000D, { 139, 8 } },
	{ 0x0200000F, { 147, 3 } },
	{ 0x02000010, { 152, 5 } },
	{ 0x02000011, { 157, 7 } },
	{ 0x02000012, { 164, 3 } },
	{ 0x02000013, { 167, 7 } },
	{ 0x02000014, { 174, 4 } },
	{ 0x02000015, { 178, 23 } },
	{ 0x02000017, { 201, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 5 } },
	{ 0x06000007, { 15, 1 } },
	{ 0x06000008, { 16, 2 } },
	{ 0x06000009, { 18, 2 } },
	{ 0x0600000A, { 20, 2 } },
	{ 0x0600000B, { 22, 1 } },
	{ 0x0600000C, { 23, 1 } },
	{ 0x0600000D, { 24, 2 } },
	{ 0x0600000E, { 26, 1 } },
	{ 0x0600000F, { 27, 2 } },
	{ 0x06000010, { 29, 3 } },
	{ 0x06000011, { 32, 2 } },
	{ 0x06000012, { 34, 1 } },
	{ 0x06000013, { 35, 7 } },
	{ 0x06000014, { 42, 4 } },
	{ 0x06000015, { 46, 4 } },
	{ 0x06000016, { 50, 4 } },
	{ 0x06000017, { 54, 3 } },
	{ 0x06000018, { 57, 3 } },
	{ 0x06000019, { 60, 1 } },
	{ 0x0600001A, { 61, 3 } },
	{ 0x0600001B, { 64, 2 } },
	{ 0x0600001C, { 66, 2 } },
	{ 0x0600001D, { 68, 5 } },
	{ 0x0600001E, { 73, 3 } },
	{ 0x0600005F, { 150, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[203] = 
{
	{ (Il2CppRGCTXDataType)2, 48977 },
	{ (Il2CppRGCTXDataType)3, 49166 },
	{ (Il2CppRGCTXDataType)2, 48978 },
	{ (Il2CppRGCTXDataType)2, 48979 },
	{ (Il2CppRGCTXDataType)3, 49167 },
	{ (Il2CppRGCTXDataType)2, 48980 },
	{ (Il2CppRGCTXDataType)2, 48981 },
	{ (Il2CppRGCTXDataType)3, 49168 },
	{ (Il2CppRGCTXDataType)2, 48982 },
	{ (Il2CppRGCTXDataType)3, 49169 },
	{ (Il2CppRGCTXDataType)2, 48983 },
	{ (Il2CppRGCTXDataType)3, 49170 },
	{ (Il2CppRGCTXDataType)3, 49171 },
	{ (Il2CppRGCTXDataType)2, 38627 },
	{ (Il2CppRGCTXDataType)3, 49172 },
	{ (Il2CppRGCTXDataType)3, 49173 },
	{ (Il2CppRGCTXDataType)2, 48984 },
	{ (Il2CppRGCTXDataType)3, 49174 },
	{ (Il2CppRGCTXDataType)2, 48985 },
	{ (Il2CppRGCTXDataType)3, 49175 },
	{ (Il2CppRGCTXDataType)2, 48986 },
	{ (Il2CppRGCTXDataType)3, 49176 },
	{ (Il2CppRGCTXDataType)3, 49177 },
	{ (Il2CppRGCTXDataType)3, 49178 },
	{ (Il2CppRGCTXDataType)2, 48987 },
	{ (Il2CppRGCTXDataType)3, 49179 },
	{ (Il2CppRGCTXDataType)3, 49180 },
	{ (Il2CppRGCTXDataType)2, 48988 },
	{ (Il2CppRGCTXDataType)3, 49181 },
	{ (Il2CppRGCTXDataType)2, 48989 },
	{ (Il2CppRGCTXDataType)3, 49182 },
	{ (Il2CppRGCTXDataType)3, 49183 },
	{ (Il2CppRGCTXDataType)2, 38662 },
	{ (Il2CppRGCTXDataType)3, 49184 },
	{ (Il2CppRGCTXDataType)3, 49185 },
	{ (Il2CppRGCTXDataType)2, 38677 },
	{ (Il2CppRGCTXDataType)3, 49186 },
	{ (Il2CppRGCTXDataType)2, 38670 },
	{ (Il2CppRGCTXDataType)2, 48990 },
	{ (Il2CppRGCTXDataType)3, 49187 },
	{ (Il2CppRGCTXDataType)3, 49188 },
	{ (Il2CppRGCTXDataType)3, 49189 },
	{ (Il2CppRGCTXDataType)2, 48991 },
	{ (Il2CppRGCTXDataType)2, 48992 },
	{ (Il2CppRGCTXDataType)2, 38678 },
	{ (Il2CppRGCTXDataType)2, 48993 },
	{ (Il2CppRGCTXDataType)2, 48994 },
	{ (Il2CppRGCTXDataType)2, 48995 },
	{ (Il2CppRGCTXDataType)2, 38680 },
	{ (Il2CppRGCTXDataType)2, 48996 },
	{ (Il2CppRGCTXDataType)2, 48997 },
	{ (Il2CppRGCTXDataType)2, 48998 },
	{ (Il2CppRGCTXDataType)2, 38682 },
	{ (Il2CppRGCTXDataType)2, 48999 },
	{ (Il2CppRGCTXDataType)2, 38684 },
	{ (Il2CppRGCTXDataType)2, 49000 },
	{ (Il2CppRGCTXDataType)3, 49190 },
	{ (Il2CppRGCTXDataType)2, 49001 },
	{ (Il2CppRGCTXDataType)2, 38687 },
	{ (Il2CppRGCTXDataType)2, 49002 },
	{ (Il2CppRGCTXDataType)2, 38689 },
	{ (Il2CppRGCTXDataType)2, 38691 },
	{ (Il2CppRGCTXDataType)2, 49003 },
	{ (Il2CppRGCTXDataType)3, 49191 },
	{ (Il2CppRGCTXDataType)2, 49004 },
	{ (Il2CppRGCTXDataType)2, 38694 },
	{ (Il2CppRGCTXDataType)2, 49005 },
	{ (Il2CppRGCTXDataType)3, 49192 },
	{ (Il2CppRGCTXDataType)3, 49193 },
	{ (Il2CppRGCTXDataType)2, 49006 },
	{ (Il2CppRGCTXDataType)2, 38698 },
	{ (Il2CppRGCTXDataType)2, 49007 },
	{ (Il2CppRGCTXDataType)2, 38700 },
	{ (Il2CppRGCTXDataType)2, 38701 },
	{ (Il2CppRGCTXDataType)2, 49008 },
	{ (Il2CppRGCTXDataType)3, 49194 },
	{ (Il2CppRGCTXDataType)3, 49195 },
	{ (Il2CppRGCTXDataType)3, 49196 },
	{ (Il2CppRGCTXDataType)2, 38707 },
	{ (Il2CppRGCTXDataType)3, 49197 },
	{ (Il2CppRGCTXDataType)3, 49198 },
	{ (Il2CppRGCTXDataType)2, 38716 },
	{ (Il2CppRGCTXDataType)2, 49009 },
	{ (Il2CppRGCTXDataType)3, 49199 },
	{ (Il2CppRGCTXDataType)3, 49200 },
	{ (Il2CppRGCTXDataType)2, 38718 },
	{ (Il2CppRGCTXDataType)2, 48765 },
	{ (Il2CppRGCTXDataType)3, 49201 },
	{ (Il2CppRGCTXDataType)3, 49202 },
	{ (Il2CppRGCTXDataType)3, 49203 },
	{ (Il2CppRGCTXDataType)2, 38725 },
	{ (Il2CppRGCTXDataType)2, 49010 },
	{ (Il2CppRGCTXDataType)3, 49204 },
	{ (Il2CppRGCTXDataType)3, 49205 },
	{ (Il2CppRGCTXDataType)3, 47505 },
	{ (Il2CppRGCTXDataType)3, 49206 },
	{ (Il2CppRGCTXDataType)3, 49207 },
	{ (Il2CppRGCTXDataType)2, 38734 },
	{ (Il2CppRGCTXDataType)2, 49011 },
	{ (Il2CppRGCTXDataType)3, 49208 },
	{ (Il2CppRGCTXDataType)3, 49209 },
	{ (Il2CppRGCTXDataType)3, 49210 },
	{ (Il2CppRGCTXDataType)3, 49211 },
	{ (Il2CppRGCTXDataType)3, 49212 },
	{ (Il2CppRGCTXDataType)3, 47511 },
	{ (Il2CppRGCTXDataType)3, 49213 },
	{ (Il2CppRGCTXDataType)3, 49214 },
	{ (Il2CppRGCTXDataType)3, 49215 },
	{ (Il2CppRGCTXDataType)2, 38754 },
	{ (Il2CppRGCTXDataType)2, 38749 },
	{ (Il2CppRGCTXDataType)3, 49216 },
	{ (Il2CppRGCTXDataType)2, 38748 },
	{ (Il2CppRGCTXDataType)2, 49012 },
	{ (Il2CppRGCTXDataType)3, 49217 },
	{ (Il2CppRGCTXDataType)3, 49218 },
	{ (Il2CppRGCTXDataType)3, 49219 },
	{ (Il2CppRGCTXDataType)3, 49220 },
	{ (Il2CppRGCTXDataType)2, 49013 },
	{ (Il2CppRGCTXDataType)3, 49221 },
	{ (Il2CppRGCTXDataType)2, 38767 },
	{ (Il2CppRGCTXDataType)2, 38759 },
	{ (Il2CppRGCTXDataType)3, 49222 },
	{ (Il2CppRGCTXDataType)3, 49223 },
	{ (Il2CppRGCTXDataType)2, 38758 },
	{ (Il2CppRGCTXDataType)2, 49014 },
	{ (Il2CppRGCTXDataType)3, 49224 },
	{ (Il2CppRGCTXDataType)3, 49225 },
	{ (Il2CppRGCTXDataType)3, 49226 },
	{ (Il2CppRGCTXDataType)2, 49015 },
	{ (Il2CppRGCTXDataType)3, 49227 },
	{ (Il2CppRGCTXDataType)2, 38780 },
	{ (Il2CppRGCTXDataType)2, 38772 },
	{ (Il2CppRGCTXDataType)3, 49228 },
	{ (Il2CppRGCTXDataType)3, 49229 },
	{ (Il2CppRGCTXDataType)3, 49230 },
	{ (Il2CppRGCTXDataType)2, 38771 },
	{ (Il2CppRGCTXDataType)2, 49016 },
	{ (Il2CppRGCTXDataType)3, 49231 },
	{ (Il2CppRGCTXDataType)3, 49232 },
	{ (Il2CppRGCTXDataType)3, 49233 },
	{ (Il2CppRGCTXDataType)2, 49017 },
	{ (Il2CppRGCTXDataType)2, 49018 },
	{ (Il2CppRGCTXDataType)3, 49234 },
	{ (Il2CppRGCTXDataType)3, 49235 },
	{ (Il2CppRGCTXDataType)2, 38792 },
	{ (Il2CppRGCTXDataType)3, 49236 },
	{ (Il2CppRGCTXDataType)2, 38793 },
	{ (Il2CppRGCTXDataType)2, 49019 },
	{ (Il2CppRGCTXDataType)3, 49237 },
	{ (Il2CppRGCTXDataType)3, 49238 },
	{ (Il2CppRGCTXDataType)2, 49020 },
	{ (Il2CppRGCTXDataType)3, 49239 },
	{ (Il2CppRGCTXDataType)2, 49021 },
	{ (Il2CppRGCTXDataType)3, 49240 },
	{ (Il2CppRGCTXDataType)3, 49241 },
	{ (Il2CppRGCTXDataType)3, 49242 },
	{ (Il2CppRGCTXDataType)2, 38812 },
	{ (Il2CppRGCTXDataType)3, 49243 },
	{ (Il2CppRGCTXDataType)2, 38820 },
	{ (Il2CppRGCTXDataType)3, 49244 },
	{ (Il2CppRGCTXDataType)2, 49022 },
	{ (Il2CppRGCTXDataType)2, 49023 },
	{ (Il2CppRGCTXDataType)3, 49245 },
	{ (Il2CppRGCTXDataType)3, 49246 },
	{ (Il2CppRGCTXDataType)3, 49247 },
	{ (Il2CppRGCTXDataType)3, 49248 },
	{ (Il2CppRGCTXDataType)3, 49249 },
	{ (Il2CppRGCTXDataType)3, 49250 },
	{ (Il2CppRGCTXDataType)2, 38836 },
	{ (Il2CppRGCTXDataType)2, 49024 },
	{ (Il2CppRGCTXDataType)3, 49251 },
	{ (Il2CppRGCTXDataType)3, 49252 },
	{ (Il2CppRGCTXDataType)2, 38840 },
	{ (Il2CppRGCTXDataType)3, 49253 },
	{ (Il2CppRGCTXDataType)2, 49025 },
	{ (Il2CppRGCTXDataType)2, 38850 },
	{ (Il2CppRGCTXDataType)2, 38848 },
	{ (Il2CppRGCTXDataType)2, 49026 },
	{ (Il2CppRGCTXDataType)3, 49254 },
	{ (Il2CppRGCTXDataType)2, 49027 },
	{ (Il2CppRGCTXDataType)3, 49255 },
	{ (Il2CppRGCTXDataType)3, 49256 },
	{ (Il2CppRGCTXDataType)3, 49257 },
	{ (Il2CppRGCTXDataType)2, 38854 },
	{ (Il2CppRGCTXDataType)3, 49258 },
	{ (Il2CppRGCTXDataType)3, 49259 },
	{ (Il2CppRGCTXDataType)2, 38857 },
	{ (Il2CppRGCTXDataType)3, 49260 },
	{ (Il2CppRGCTXDataType)1, 49028 },
	{ (Il2CppRGCTXDataType)2, 38856 },
	{ (Il2CppRGCTXDataType)3, 49261 },
	{ (Il2CppRGCTXDataType)1, 38856 },
	{ (Il2CppRGCTXDataType)1, 38854 },
	{ (Il2CppRGCTXDataType)2, 49029 },
	{ (Il2CppRGCTXDataType)2, 38856 },
	{ (Il2CppRGCTXDataType)2, 38859 },
	{ (Il2CppRGCTXDataType)2, 38858 },
	{ (Il2CppRGCTXDataType)3, 49262 },
	{ (Il2CppRGCTXDataType)3, 49263 },
	{ (Il2CppRGCTXDataType)3, 49264 },
	{ (Il2CppRGCTXDataType)2, 38855 },
	{ (Il2CppRGCTXDataType)3, 49265 },
	{ (Il2CppRGCTXDataType)2, 38869 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	144,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	44,
	s_rgctxIndices,
	203,
	s_rgctxValues,
	NULL,
};
